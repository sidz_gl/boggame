﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace SIDZ.BogGame.Core
{
    public class BogMainMenu : MonoBehaviour
    {
        [SerializeField] private GameDetails m_refGameDetails;

        [Header("UI:Single Create")]
        [SerializeField] private InputField m_UIspGameName;
        [SerializeField] private InputField m_UIspGameSize;
        [SerializeField] private InputField m_UIspGameTime;

        [Header("UI:Mulitplayer Create")]
        [SerializeField] private InputField m_UIGameName;
        [SerializeField] private InputField m_UIGameSize;
        [SerializeField] private InputField m_UIGameTime;

        [Header("UI:Mulitplayer Join")]
        [SerializeField] private InputField m_UIRoomIP;
        [SerializeField] private CustomNetworkDiscovery customNetworkDiscovery;

        // Use this for initialization
        void Start()
        {
            //DontDestroyOnLoad(this);
            if (BogAudioManager._Instance)
                BogAudioManager._Instance.PlayMusic(0, true);

            Cursor.visible = true;
            Input.multiTouchEnabled = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        #region UI
        public void StartSinglePlayer()
        {
            m_refGameDetails._GameType = BogCommonHelper.eGameType.SinglePlayer;
            GameDetails.SessionData temp = new GameDetails.SessionData();

            temp.m_GameType = BogCommonHelper.eGameType.SinglePlayer;
            temp.multiType = GameDetails.eMultiType.Host;
            temp.eSessionType = GameDetails.eSessionType.Release;

            temp.gameSize = 1;
            temp.roomName = m_UIspGameName.text;
            temp.gameTimer = int.Parse(m_UIspGameTime.text);
            m_refGameDetails.CreateSession(temp);

            if (m_refGameDetails._CustomLobbyManager != null)
            {
                m_refGameDetails._CustomLobbyManager.SetupGame();
                Debug.Log("BogMainMenu:Lobby Already there...Using SAme to start it up");
            }

            UnityEngine.SceneManagement.SceneManager.LoadScene("GameLobbyScene");
        }

        public void StartMultiPlayer()
        {
            m_refGameDetails._GameType = BogCommonHelper.eGameType.MultiPlayer;
            GameDetails.SessionData temp = new GameDetails.SessionData();

            temp.m_GameType = BogCommonHelper.eGameType.MultiPlayer;
            temp.multiType = GameDetails.eMultiType.Host;
            temp.eSessionType = GameDetails.eSessionType.Release;

            temp.roomName = m_UIGameName.text;
            temp.gameSize = int.Parse(m_UIGameSize.text);
            temp.gameTimer = int.Parse(m_UIGameTime.text);

            m_refGameDetails.CreateSession(temp);

            if (m_refGameDetails._CustomLobbyManager != null)
            {
                m_refGameDetails._CustomLobbyManager.SetupGame();
                Debug.Log("BogMainMenu:Lobby Already there...Using SAme to start it up");
            }

            UnityEngine.SceneManagement.SceneManager.LoadScene("GameLobbyScene");

        }
        public void JoinMultiPlayer()
        {

            m_refGameDetails._GameType = BogCommonHelper.eGameType.MultiPlayer;
            GameDetails.SessionData temp = new GameDetails.SessionData();

            temp.m_GameType = BogCommonHelper.eGameType.MultiPlayer;
            temp.multiType = GameDetails.eMultiType.Client;
            temp.eSessionType = GameDetails.eSessionType.Release;
            


            temp.m_refIPHost = m_UIRoomIP.text;

            m_refGameDetails.CreateSession(temp);

            if (m_refGameDetails._CustomLobbyManager != null)
            {
                m_refGameDetails._CustomLobbyManager.SetupGame();
                Debug.LogError("Let's not do this... BogMainMenu:Lobby Already there...Using SAme to start it up");
            }

            UnityEngine.SceneManagement.SceneManager.LoadScene("GameLobbyScene");

        }

        public void QuitGame()
        {
            Application.Quit();
        }

        #endregion UI
    }
}