﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SIDZ.BogGame.Core
{
    [System.Serializable]
    public class PlayerScoreSet
    {
        public string m_strName = "Default Name";
        public List<string> m_lstString;
        public int m_iScore = 0;
        public string m_strNetID = "-99999";
        public string IP_Address = "localhost";


        public PlayerScoreSet()
        {
            m_strName = "Default Name";
            m_lstString = new List<string>();
            m_strNetID = "-99999";
            m_iScore = 0;
        }

        private bool IsValid()
        {
            return m_lstString != null;
        }

        public bool IsWordPresent(string a_strWordToCheck)
        {
            return IsValid() && m_lstString.Contains(a_strWordToCheck);
        }

        public void RemoveWord(string a_strWordToRemove)
        {
            if (IsValid() && m_lstString.Contains(a_strWordToRemove))
            {
                m_lstString.Remove(a_strWordToRemove);
            }
        }

        public void AddWord(string a_strWordToAdd)
        {
            if (IsValid() && !m_lstString.Contains(a_strWordToAdd))
            {
                m_lstString.Add(a_strWordToAdd);
            }
        }

        public void AddPlayerWordList(List<string> a_lstWordList)
        {
            if (IsValid())
            {
                m_lstString = new List<string>();
            }
            m_lstString.Clear();
            m_lstString.AddRange(a_lstWordList);
        }

        public void ClearWords()
        {
            if (IsValid())
            {
                m_lstString.Clear();
            }
        }

    }

    /// <summary>
    /// Takes care of displaying all the scores of the player 
    /// and also checking if commom stuff is there and alloting scores...
    /// </summary>
    public class BogScoreDisplay : MonoBehaviour
    {

        [SerializeField] private GameDetails m_refGameDetails;
        [SerializeField] private BogUIWordList m_LocalBogWordList;

        [SerializeField] private int m_iPlayerCount = 1;
        [Header("Read only")]
        [SerializeField] private List<PlayerScoreSet> m_lstPlayerScoreSet;
        [SerializeField] private Text a_refUIScoreText;
        [SerializeField] private Text m_UIDisplayScore;

        [Header("UI FX")]
        [SerializeField] private Transform m_refOtherPlayerBox;
        [SerializeField] private Transform m_refScoreDisplayBox;
        [SerializeField] private Text m_refPrefabGameObject;
        [Space(4)]
        [SerializeField] private Text m_refPlayerWord;
        [SerializeField] private Text m_refWordScore;
        [SerializeField] private Animator m_refScoreEffectArea;
        [SerializeField] private RectTransform m_refOtherPlayerScrollBox;
        [SerializeField] private Text m_refOtherPlayerNOBodySelected;

        [Header("Audio_LAzy to keep it seperate")]
        [SerializeField] private AudioSource m_refAudioSSource;
        [SerializeField] private AudioClip m_sfxCorrectWord;
        [SerializeField] private AudioClip m_sfxWrongWord;
        [SerializeField] private AudioClip m_sfxPlayerMatch;


        private int a_iPlayerDataSyncReadyCount = 0;
        private bool a_bPlayerDataSyncReady = false;


        void PlayClip(AudioClip ma_ClipToPlay)
        {
            m_refAudioSSource.clip = ma_ClipToPlay;
            m_refAudioSSource.Play();
        }

        // Use this for initialization
        void OnEnable()
        {
            BogEventManager._Instance.EvntOnOtherPlayerDataRcvd += OnOtherPlayerDataRcvd;
        }
        void OnDisable()
        {
            BogEventManager._Instance.EvntOnOtherPlayerDataRcvd -= OnOtherPlayerDataRcvd;
        }

        private void OnOtherPlayerDataRcvd(string a_strName, string a_strConnID)
        {
            a_iPlayerDataSyncReadyCount++;
            if (a_iPlayerDataSyncReadyCount == m_refGameDetails._LstOtherPlayers.Count)
            {
                a_bPlayerDataSyncReady = true;
            }
        }

        void Start()
        {
            Init();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void Init()
        {
            if (m_iPlayerCount == 0)
            {
                return;
            }

            for (int i = 0; i < m_iPlayerCount; i++)
            {
                m_lstPlayerScoreSet.Add(new PlayerScoreSet());
            }

            UpdateScoreForLocalPlayer(m_refGameDetails._LocalPlayer.m_iScore);


        }

        //Creates ui and stuff
        public void HideDisplay()
        {
            a_refUIScoreText.gameObject.SetActive(false);
            m_refOtherPlayerBox.gameObject.SetActive(false);
            m_refScoreDisplayBox.gameObject.SetActive(false);
        }
        public void Display(List<PlayerScoreSet> a_lstOtherPlayersScoreSet)
        {

            m_refOtherPlayerBox.gameObject.SetActive(true);
            m_refScoreDisplayBox.gameObject.SetActive(true);

            if (a_lstOtherPlayersScoreSet == null)
            {
                Debug.Log("Single Player Only just display everthing and score only");
                SinglePlayerLogic();
            }
            else
            {
                Debug.Log("Mutli Player Only do checking");
            }

            //Display words
            //Check if more players are there
            //Cross each word out
        }
        Coroutine m_refSinglePlayer = null;
        private void SinglePlayerLogic()
        {
            //  m_refSinglePlayer = StartCoroutine(coSinglePlayer());
            m_refSinglePlayer = StartCoroutine(coScoreCalculator());

        }
        IEnumerator coSinglePlayer()
        {
            m_refGameDetails._GameManager.ToggleInteractStateButton(false);

            a_refUIScoreText.gameObject.SetActive(true);
            var temp_locWordList = m_LocalBogWordList.GetAllWord();

            int iScore = 0;

            Animator m_refScoreAnimator = a_refUIScoreText.GetComponent<Animator>();
            a_refUIScoreText.text = "" + iScore;
            m_refScoreAnimator.SetTrigger("ScoreWord");
            yield return new WaitForSeconds(1);
            foreach (string temp_word in temp_locWordList)
            {
                //Scoring rules
                iScore += GetScoreBasedOnWordLenght(temp_word.Length);
                m_refScoreAnimator.SetTrigger("WrongWord");
                a_refUIScoreText.text = "" + iScore;
                yield return new WaitForSeconds(0.01f);
            }
            yield return new WaitForSeconds(0.2f);



            /*
             Go to list of local players
             create UI for them
             Now hightlight text this players and other players list and remove from self
             no that for all our word list
             */


            //Wait for all the player string to be recved
            while (a_bPlayerDataSyncReady == false && m_refGameDetails._LstOtherPlayers.Count != 0)
            {
                yield return new WaitForEndOfFrame();
            }
            a_bPlayerDataSyncReady = false;
            a_iPlayerDataSyncReadyCount = 0;
            Debug.LogError("All players sync will start doing calculation against other players..");
            int iReduceScore = 0;



            int iOtherScore = 0;
            foreach (PlayerScoreSet tempScoreSet in m_refGameDetails._LstOtherPlayers)
            {
                iOtherScore = 0;
                foreach (string temp_word in tempScoreSet.m_lstString)
                {
                    //Scoring rules
                    iOtherScore += GetScoreBasedOnWordLenght(temp_word.Length);
                    m_refScoreAnimator.SetTrigger("WrongWord");
                }
                tempScoreSet.m_iScore += iOtherScore;

                foreach (string m_strSelectedString in tempScoreSet.m_lstString)
                {
                    if (temp_locWordList.Contains(m_strSelectedString))
                    {
                        iReduceScore = GetScoreBasedOnWordLenght(m_strSelectedString.Length);
                        iScore -= iReduceScore;
                        tempScoreSet.m_iScore -= iReduceScore;
                        m_refScoreAnimator.SetTrigger("ScoreWord");
                        a_refUIScoreText.text = "" + iScore;
                        Debug.Log("Player Name:" + tempScoreSet.m_strName + "has same word as you:" + m_strSelectedString);
                        //REmoving from your list !!
                        temp_locWordList.Remove(m_strSelectedString);
                        Debug.LogError("Player Name:" + tempScoreSet.m_strName + "has same word as you:" + m_strSelectedString + "REMOVED!");
                        yield return new WaitForSeconds(2.0f);

                    }
                }
            }

            yield return new WaitForSeconds(0.2f);

            UpdateScoreForLocalPlayer(iScore);



            m_refGameDetails._GameManager.ToggleInteractStateButton(true);
        }
        private void BaseLogicMultiPlayer()
        {


        }
        private void UpdateScoreForLocalPlayer(int a_iScore)
        {
            //Add score to game detials
            m_refGameDetails._LocalPlayer.m_iScore += a_iScore;
            m_UIDisplayScore.text = m_refGameDetails._LocalPlayer.m_iScore + "";
        }

        private int GetScoreBasedOnWordLenght(int a_strWordLenght)
        {
            switch (a_strWordLenght)
            {
                case 1:

                    return 1;
                case 2:
                    return 1;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 2;
                case 6:
                    return 4;
                case 7:
                    return 8;
                case 8:
                    return 16;
                case 9:
                    return 32;
                default:
                    return 64;

            }
            return 0;
        }

        #region UI_FX
        private void AddSameWordedPlayer()
        {

        }
        private void RemoveSameWordedPlayer()
        {

        }
        private void ClearAllSameWordedPlayer()
        {

        }
        IEnumerator coScoreCalculator()
        {
            m_refOtherPlayerNOBodySelected.gameObject.SetActive(false);
            m_refGameDetails._GameManager.ToggleInteractStateButton(false);
            //Wait for all the player string to be recved
            //In case of multiplayer wait to get all strings from others...
            while (a_bPlayerDataSyncReady == false && m_refGameDetails._LstOtherPlayers.Count != 0 && false)
            {
                yield return new WaitForEndOfFrame();
            }
            a_bPlayerDataSyncReady = false;
            a_iPlayerDataSyncReadyCount = 0;
            Debug.LogError("All players sync will start doing calculation against other players..");





            a_refUIScoreText.gameObject.SetActive(true);
            var temp_locWordList = m_LocalBogWordList.GetAllWord();

            int iScore = 0;
            bool m_bSkipMultiplayer = m_refGameDetails._LstOtherPlayers == null || m_refGameDetails._LstOtherPlayers.Count == 0;


            a_refUIScoreText.text = "" + iScore;

            yield return new WaitForSeconds(1);

            Text temp_text = null;
            List<Text> temp_createdText = new List<Text>();

            foreach (string temp_word in temp_locWordList)
            {
                //Scoring rules

                //Updates the Word TEXT
                m_refOtherPlayerNOBodySelected.gameObject.SetActive(false);
                yield return new WaitForSeconds(0.5f);
                m_refPlayerWord.text = temp_word;


                if (m_bSkipMultiplayer == false || true)
                {
                    //REMOVE OFF the previous display of Players...
                    foreach (Text temp_Text in temp_createdText)//TODO bad for now as we keep destroying but it's ok
                    {
                        
                        Destroy(temp_Text.gameObject);
                    }
                    if (temp_createdText != null)
                    {
                        temp_createdText.Clear();
                    }

                    foreach (PlayerScoreSet m_otherPlayerSet in m_refGameDetails._LstOtherPlayers)
                    {
                        if (m_otherPlayerSet.IsWordPresent(temp_word))
                        {
                            //ADD player..
                            //  m_refOtherPlayerScrollBox
                            temp_text = Instantiate(m_refPrefabGameObject.gameObject, m_refOtherPlayerScrollBox.transform).GetComponent<Text>();
                            temp_text.text = m_otherPlayerSet.m_strName;
                            PlayClip(m_sfxPlayerMatch);
                            yield return new WaitForSeconds(1.5f);
                            temp_createdText.Add(temp_text);

                        }

                    }
                    if (temp_createdText.Count !=0 )
                    {
                        m_refScoreEffectArea.SetTrigger("Trigger_SameWord");
                        PlayClip(m_sfxWrongWord);
                        yield return new WaitForSeconds(2.0f);
                        m_refPlayerWord.text = "";
                        m_refScoreEffectArea.SetTrigger("Trigger_Idle");
                    }
                    else//no matchin words found in others,,,
                    {
                        m_refOtherPlayerNOBodySelected.gameObject.SetActive(true);
                        m_refWordScore.text = "" + GetScoreBasedOnWordLenght(temp_word.Length);

                        //animate and wait it out...(same as below for single  player refactor later..)
                        PlayClip(m_sfxCorrectWord);
                        m_refScoreEffectArea.SetTrigger("Trigger_AddScore");
                      yield return new WaitForSeconds(m_refScoreEffectArea.GetCurrentAnimatorStateInfo(0).length+2);
                        iScore += GetScoreBasedOnWordLenght(temp_word.Length);
                        UpdateScoreForLocalPlayer(GetScoreBasedOnWordLenght(temp_word.Length));
                    //    yield return new WaitForSeconds(2f);
                        m_refScoreEffectArea.SetTrigger("Trigger_Idle");

                    }

                    //Add the text
                    //Check if other players have em
                    //display those names
                    //Do wrong
                    //else what ever happend below do same..
                }
                else
                {

                    //animate and wait it out...
                    m_refScoreEffectArea.SetTrigger("Trigger_AddScore");
                    yield return new WaitForSeconds(m_refScoreEffectArea.GetCurrentAnimatorStateInfo(0).length);
                    m_refWordScore.text = "" + GetScoreBasedOnWordLenght(temp_word.Length);
                    iScore += GetScoreBasedOnWordLenght(temp_word.Length);
                    UpdateScoreForLocalPlayer(GetScoreBasedOnWordLenght(temp_word.Length));
                    yield return new WaitForSeconds(2f);
                    m_refScoreEffectArea.SetTrigger("Trigger_Idle");
                }



            }
            yield return new WaitForSeconds(0.2f);

            //   UpdateScoreForLocalPlayer(iScore);
            m_refGameDetails._GameManager.ToggleInteractStateButton(true);
            foreach (Text temp_Text in temp_createdText)//TODO bad for now as we keep destroying but it's ok
            {

                Destroy(temp_Text.gameObject);
            }
            if (temp_createdText != null)
            {
                temp_createdText.Clear();
            }
            m_refWordScore.text = "";
            m_refPlayerWord.text = "";

            /*
             Go to list of local players
             create UI for them
             Now hightlight text this players and other players list and remove from self
             no that for all our word list
             */



            //int iReduceScore = 0;

            //int iOtherScore = 0;
            //foreach (PlayerScoreSet tempScoreSet in m_refGameDetails._LstOtherPlayers)
            //{
            //    iOtherScore = 0;
            //    foreach (string temp_word in tempScoreSet.m_lstString)
            //    {
            //        //Scoring rules
            //        iOtherScore += GetScoreBasedOnWordLenght(temp_word.Length);
            //        //  m_refScoreAnimator.SetTrigger("WrongWord");
            //    }
            //    tempScoreSet.m_iScore += iOtherScore;

            //    foreach (string m_strSelectedString in tempScoreSet.m_lstString)
            //    {
            //        if (temp_locWordList.Contains(m_strSelectedString))
            //        {
            //            iReduceScore = GetScoreBasedOnWordLenght(m_strSelectedString.Length);
            //            iScore -= iReduceScore;
            //            tempScoreSet.m_iScore -= iReduceScore;
            //            // m_refScoreAnimator.SetTrigger("ScoreWord");
            //            a_refUIScoreText.text = "" + iScore;
            //            Debug.Log("Player Name:" + tempScoreSet.m_strName + "has same word as you:" + m_strSelectedString);
            //            //REmoving from your list !!
            //            temp_locWordList.Remove(m_strSelectedString);
            //            Debug.LogError("Player Name:" + tempScoreSet.m_strName + "has same word as you:" + m_strSelectedString + "REMOVED!");
            //            yield return new WaitForSeconds(2.0f);

            //        }
            //    }
            //}

            //yield return new WaitForSeconds(0.2f);

            //Check if mutiplayer is on
          //  if(network)
         //   m_refGameDetails._GameManager.NextStates();

        }
        #endregion UI_FX
    }
}