﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    public class BogEventManager : MonoBehaviour
    {

        private static BogEventManager _instance;
        private BogEventManager()
        {

        }

        public static BogEventManager _Instance
        {
            get
            {

                if (_instance == null)
                {
                    _instance = FindObjectOfType<BogEventManager>();
                }
                return _instance;
            }

        }
        void Awake()
        {
            //Check if instance already exists
            if (_instance == null)

                //if not, set instance to this
                _instance = this;

            //If instance already exists and it's not this:
            else if (_instance != this)

                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

        }

        #region NETWORKING
        public delegate void OnRoomFound(string a_strRoomName,string a_strRoomIP,string a_strRoomPort);
        public OnRoomFound EvntOnRoomFound;

        public void Fire_EvntOnRoomFound(string a_strRoomName, string a_strRoomIP, string a_strRoomPort)
        {
            if(EvntOnRoomFound !=null)
            {
                EvntOnRoomFound(a_strRoomName, a_strRoomIP, a_strRoomPort);
            }
        }
        /// <summary>
        /// Listens to Net if another player data is rcvd.....
        /// </summary>
        /// <param name="a_strName"></param>
        /// <param name="a_strConnID"></param>
        public delegate void OnOtherPlayerDataRcvd(string a_strName, string a_strConnID);
        public OnOtherPlayerDataRcvd EvntOnOtherPlayerDataRcvd;

        public void Fire_EvntOnOtherPlayerDataRcvd(string a_strName, string a_strConnID)
        {
            if (EvntOnOtherPlayerDataRcvd != null)
            {
                EvntOnOtherPlayerDataRcvd(a_strName, a_strConnID);
            }
        }


        #endregion NETWORKING

        #region UI_LIST_BOX
        //THese event are only for UI _LIST BOX...
        //Delegate Declaration
        public delegate void  OnUI_CorrectWord(string a_strWord);
        public OnUI_CorrectWord EvntOnUI_CorrectWord;

        public delegate void OnUI_RemoveWord(string a_strWord);
        public OnUI_RemoveWord evntOnUI_RemoveWord;

        public delegate void BogShuffle();
        public BogShuffle EvntOnUI_BogShuffle;

        public delegate void BogShuffleComplete();
        public BogShuffleComplete EvntOnUI_BogShuffleComplete;
        public void Fire_evntOnUI_BogShuffleComplete()
        {
            if (EvntOnUI_BogShuffleComplete != null)
            {
                EvntOnUI_BogShuffleComplete();
            }
        }



        public void  Fire_evntOnUI_CorrectWord(string a_strWord)
        {
            if(EvntOnUI_CorrectWord!=null)
            {
                EvntOnUI_CorrectWord(a_strWord);
            }
        }


        public void Fire_evntOnUI_BogShuffle()
        {
            if (EvntOnUI_BogShuffle != null)
            {
                EvntOnUI_BogShuffle();
            }
        }

        #endregion UI_LIST_BOX
        #region COMMON

        public delegate void OnTimerOver(bool a_bIsTimeElasped);
        public OnTimerOver EventOnTimerOver;
        public void Fire_EventOnTimerOver(bool a_bIsForceStop)
        {
            if(EventOnTimerOver!=null)
            {
                EventOnTimerOver(a_bIsForceStop);
            }
        }


        /*Player Joined
         Player Left*/

        public delegate void OnPlayerJoinedGame(PlayerInfo a_refPlayerInfo);
        public OnPlayerJoinedGame EvntOnPlayerJoinedGame;

        public void Fire_EvntOnPlayerJoinedGame(PlayerInfo a_refPlayerInfo)
        {
            if(EvntOnPlayerJoinedGame!=null)
            {
                EvntOnPlayerJoinedGame(a_refPlayerInfo);
            }
        }
        public delegate void OnPlayerLeftGame(PlayerInfo a_refPlayerInfo);
        public OnPlayerLeftGame EvntOnPlayerLeftGame;

        public void Fire_EvntOnPlayerLeftGame(PlayerInfo a_refPlayerInfo)
        {
            if (EvntOnPlayerLeftGame != null)
            {
                EvntOnPlayerLeftGame(a_refPlayerInfo);
            }
        }


        /*
         STate System
         Idle*/
        /*
       public delegate void OnGameStateChange(IStateSystem a_NewState);
       public OnGameStateChange EvntOnGameStateChange;

       public void Fire_EvntOnGameStateChange(IStateSystem a_NewState)
       {
           if(EvntOnGameStateChange == null)
           {
               EvntOnGameStateChange(a_NewState);
           }
       }
      */

        public delegate void OnGameStateChange(EGameState a_NewState);
        public OnGameStateChange EvntOnGameStateChange;

        public void Fire_EvntOnGameStateChange(EGameState a_NewState)
        {
            if (EvntOnGameStateChange != null)
            {
                EvntOnGameStateChange(a_NewState);
            }
        }


        #endregion COMMON
    }
}