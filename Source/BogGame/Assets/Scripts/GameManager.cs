﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SIDZ.BogGame.Core
{
    public enum EGameState
    {
        StartRoundState,
        ShuffleState,
        PlayingState,
        ScoreDisplayState
    }
    public class GameManager : MonoBehaviour, IStateObject
    {
        [Header("Game Scene Tools")]
        [SerializeField] private GameDetails m_GameDetails;



        #region STATES
        [SerializeField] private Text m_refButtonText;
        [Header("ReadlOnly")]
        [SerializeField] private BogCommonHelper.eActiveState m_MyState = BogCommonHelper.eActiveState.Enabled;

        [Header("GameManager: States")]
        [SerializeField] private EGameState m_eCurrentState = EGameState.StartRoundState;
        [SerializeField] private EGameState m_ePreviousState = EGameState.StartRoundState;

        [Header("Dependencies")]
        [SerializeField] private BogBoard m_refBogBoard;
        [SerializeField] private BogPointer m_refBogPointer;
        [SerializeField] private Button m_refStateChangeButton;


        [Header("Player Details")]
        [SerializeField] private PlayerScoreSet m_LocalPlayer;
        [SerializeField] private List<PlayerScoreSet> m_lstOtherPlayers;
        [SerializeField] private Text m_UIDisplayName;


        [Header("SCore Display")]
        [SerializeField] private BogScoreDisplay m_refBogScoreDisplay;
        [SerializeField] private BogUIWordList m_refBogUIWordList;

        [Header("Timer Panel")]
        [SerializeField] private BogTimer m_refBogTimer;



        #endregion STATES
        // Use this for initialization
        void Start()
        {
            Init();
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                NextStates();
            }
        }
        void OnDisable()
        {
            BogEventManager._Instance.EvntOnGameStateChange -= OnGameStateChange;
            m_refBogTimer.RemoveListener(OnTimerComplete);
        }

        private void Init()
        {
            m_GameDetails._GameManager = this;

            m_eCurrentState = EGameState.StartRoundState;
            m_ePreviousState = EGameState.StartRoundState;
            m_refButtonText.text = "Shuffle Bog";
            BogEventManager._Instance.EvntOnGameStateChange += OnGameStateChange;
            BogEventManager._Instance.EvntOnUI_BogShuffleComplete += OnShuffleComplete;
            m_refBogPointer.SetActiveState(BogCommonHelper.eActiveState.Disabled);


            m_UIDisplayName.text = m_GameDetails._LocalPlayer.m_strName;


            if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Client)
            {
                m_refStateChangeButton.gameObject.SetActive(false);
                Debug.LogError("***Since its a client the m_refStateChangeButton is DISABLED");
            }
            else
            {
                Debug.LogError("***Since its a HOST the m_refStateChangeButton is ENABLED");
            }
            if (BogAudioManager._Instance)
                BogAudioManager._Instance.PlayMusic(1, true);

            m_refBogTimer.gameObject.SetActive(true);
            m_refBogTimer.AddListener(OnTimerComplete);
         
        }

        private void OnShuffleComplete()
        {
            Debug.LogError("Shuffel complete EVent Fired..hooked to Game Managewr!");
            //Should not happen for clients in network mode...
            if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
            {
                NextStates();
                Debug.LogError(" HOST so calling NextStates event");
            }
            else
            {
                Debug.LogError(" Client so calling NextStatesevent disabled");
            }
        }



        #region STATES


        public void NextStates()
        {
            ChangeToNext();
        }
        /// <summary>
        /// This func is used to change to a STATE!
        ///Mostly for NEt this was made
        /// </summary>
        /// <param name="a_eNewState"></param>
        public void ChangeToState(EGameState a_eNewState)
        {
            if (a_eNewState == m_eCurrentState)
            {
                Debug.LogError("ChangeToState:Same state change called!" + a_eNewState.ToString());
                return;
            }
            m_ePreviousState = m_eCurrentState;
            m_eCurrentState = a_eNewState;
            BogEventManager._Instance.Fire_EvntOnGameStateChange(m_eCurrentState);
            Debug.Log("ChangeToState:Completed:NewState:" + m_eCurrentState.ToString() + ",OldState:" + m_ePreviousState.ToString());
        }


        private void ChangeToNext()
        {
            switch (m_eCurrentState)
            {
                case EGameState.StartRoundState:
                    m_ePreviousState = m_eCurrentState;
                    m_eCurrentState = EGameState.ShuffleState;

                    break;
                case EGameState.ShuffleState:
                    m_ePreviousState = m_eCurrentState;
                    m_eCurrentState = EGameState.PlayingState;


                    break;
                case EGameState.PlayingState:
                    m_ePreviousState = m_eCurrentState;
                    m_eCurrentState = EGameState.ScoreDisplayState;

                    break;
                case EGameState.ScoreDisplayState:
                    m_ePreviousState = m_eCurrentState;
                    m_eCurrentState = EGameState.StartRoundState;

                    break;

            }
            BogEventManager._Instance.Fire_EvntOnGameStateChange(m_eCurrentState);
        }
        private void OnTimerComplete(bool a_bTimerStoppedByForce)
        {

            if (m_GameDetails._CurrentSessionData.multiType != GameDetails.eMultiType.Host)
            {
                Debug.LogError("CAn be summoned only by host..");
                return;
            }
            if (m_eCurrentState != EGameState.PlayingState)
            {
                Debug.LogError("OnTimerComplete:Not in playing state nothing to do after timer...");
                return;
            }
            if (a_bTimerStoppedByForce == true)
            {
                Debug.LogError("OnTimerComplete:Some1 Force stopped not doing anythting...");
                return;
            }
            NextStates();

        }

        //GAME logic base on state change
        private void OnGameStateChange(EGameState a_NewState)
        {
            if (IsActiveState() == false)
            {
                Debug.Log("Game manager not Active" + m_MyState);
                return;
            }

            switch (a_NewState)
            {
                case EGameState.StartRoundState:
                    m_refButtonText.transform.parent.GetComponent<Button>().interactable = true;
                    m_refBogScoreDisplay.HideDisplay();
                    m_refBogUIWordList.ClearWordList();
                    m_refBogPointer.SetActiveState(BogCommonHelper.eActiveState.Disabled);
                    m_refButtonText.text = "Shuffle Bog";
                    m_GameDetails.ClearAllWordList();
                    // Timer
                    m_refBogTimer.gameObject.SetActive(false);


                    break;
                case EGameState.ShuffleState:
                    m_refButtonText.transform.parent.GetComponent<Button>().interactable = false;
                    m_refBogBoard.ShuffleCube(5, false);//Keep it constant
                    m_refBogPointer.SetActiveState(BogCommonHelper.eActiveState.Disabled);
                    m_refButtonText.text = "Start Playing";




                    break;
                case EGameState.PlayingState:
                    m_refButtonText.transform.parent.GetComponent<Button>().interactable = true;
                    m_refBogPointer.SetActiveState(BogCommonHelper.eActiveState.Enabled);
                    m_refButtonText.text = "End Round";
                    //Start Timer
                    m_refBogTimer.gameObject.SetActive(true);
                    m_GameDetails._BogTimer._AttachedAnimator.SetTrigger("Trigger_StartTimer");
                    if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
                    {
                        m_refBogTimer.gameObject.SetActive(true);
                        m_refBogTimer.StartTimer(m_GameDetails._CurrentSessionData.gameTimer);
                    }
                    else
                    {
                        Debug.Log("wait for host to start");
                    }
                    break;
                case EGameState.ScoreDisplayState:
                    m_refButtonText.transform.parent.GetComponent<Button>().interactable = true;
                    m_refBogPointer.SetActiveState(BogCommonHelper.eActiveState.Disabled);
                    m_refButtonText.text = "Start Round";

                    m_refBogTimer.StopTimer(true);
                    //Get all player scores...
                    //Single player tap into bogpointer....

                    m_LocalPlayer.m_lstString.Clear();
                    m_LocalPlayer.AddPlayerWordList(m_refBogUIWordList.GetAllWord());
                    m_refBogScoreDisplay.Display(null); ;
                    // Timer
                    m_refBogTimer.gameObject.SetActive(false);




                    break;

            }
        }
        public void ToggleInteractStateButton(bool m_bInteractState)
        {
            m_refButtonText.transform.parent.GetComponent<Button>().interactable = m_bInteractState;
        }
        public bool IsActiveState()
        {
            return (m_MyState == BogCommonHelper.eActiveState.Enabled);
        }

        public BogCommonHelper.eActiveState GetCurrentState()
        {
            return (m_MyState);
        }

        public void SetActiveState(BogCommonHelper.eActiveState a_eCurrentActiveState)
        {
            m_MyState = a_eCurrentActiveState;
        }

        #endregion STATES

        public void BtnHommeButton()
        {
            m_GameDetails._CustomLobbyManager.StopGame();
        }
    }
}