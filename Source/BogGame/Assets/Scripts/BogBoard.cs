﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    public class BogBoard : MonoBehaviour
    {
        [ContextMenu("Test Me")]
        public void TestMe()
        {
            //   Random.InitState(m_iSeed);

            foreach (BogDice m_tempDice in m_lstBogDice)
            {
                DestroyImmediate(m_tempDice.gameObject);
            }
            m_lstBogDice.Clear();

            CreateBoard();
        }

        [Header("Settings:BogBoard")]
        [SerializeField] private BogDice m_refGameObject;
        [SerializeField] private Transform m_refDiceHolderParent;
        [SerializeField] private BogResourceHolder m_refBogResource;
        [SerializeField] private BogDice[,] m_arrayGrid;
        [SerializeField] private List<BogDice> m_lstBogDice;
        [Range(1, 10)]
        [SerializeField] private int m_iXsize = 4;
        [Range(1, 10)]
        [SerializeField] private int m_iYsize = 4;
        [Range(0.1f, 10)]
        [SerializeField] private float iXSpace = 2;
        [Range(0.1f, 10)]
        [SerializeField] private float iYSpace = 2;

        [Header("Settings:Random")]
        [SerializeField] private int m_iSeed = 5;
        [SerializeField] private List<string> m_lstRandomDiceArray;

        [Header("Settings:Player")]
        [SerializeField] private BogPointer m_refPointer;
        [SerializeField] private Camera m_refGameCamera;
        private GameObject m_goPointer;

        


        private Transform m_transMouse = null;
        private Vector3 m_vCachNewVector= Vector3.zero;

        // Use this for initialization
        void Start()
        {
         //   Cursor.visible = false;
            ///RANDOM will come here
            ///
            Debug.LogError("Random number will come here");
            Init();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                CreateBoard();
            }


            //Mouse Update
            m_vCachNewVector = m_refGameCamera.ScreenToWorldPoint(Input.mousePosition);
            m_vCachNewVector.z = 0.1f;
            m_transMouse.position = m_vCachNewVector;


        }

        #region BOG_BOARD_LOGIC

        public void ShuffleCube(int a_iSeed,bool a_bRefreshSeed)
        {
            if(a_bRefreshSeed)
                UnityEngine.Random.InitState(a_iSeed);
            CreateBoard();

        }
        void OnDisable()
        {
            BogEventManager._Instance.EvntOnGameStateChange -= OnGameStateChangeLogic;
        }
        

        private void Init()
        {
            m_transMouse = m_refPointer.transform;
            m_vCachNewVector = new Vector3();
            //   CreateBoard();
            BogEventManager._Instance.EvntOnGameStateChange += OnGameStateChangeLogic;
          
        }

      

        private void CreateBoard()
        {
            ShowBoardDice();

            BogEventManager._Instance.Fire_evntOnUI_BogShuffle();

            m_lstRandomDiceArray.Clear();
            //Randomlyy create list of 16 numbers
            for (int i = 0; i < m_iXsize; i++)
            {
                for (int j = 0; j < m_iYsize; j++)
                {
                    m_lstRandomDiceArray.Add(BogCommonHelper.const_arayCube4x4[i, j]);
                }
            }
            //shuffle this list
            string temp_str1, temp_str2;
            int iRandomIndex = 0;
            for (int i = 0; i < m_lstRandomDiceArray.Count; i++)
            {
                temp_str1 = m_lstRandomDiceArray[i];
                iRandomIndex = UnityEngine.Random.Range(i, m_lstRandomDiceArray.Count);
                temp_str2 = m_lstRandomDiceArray[iRandomIndex];
                m_lstRandomDiceArray[i] = temp_str2;
                m_lstRandomDiceArray[iRandomIndex] = temp_str1;
            }


            if (m_lstBogDice != null && m_lstBogDice.Count > 0)
            {
                foreach (BogDice m_tempDice in m_lstBogDice)
                {
                    Destroy(m_tempDice.gameObject);
                }
                m_lstBogDice.Clear();
            }

            if (m_refGameObject == null)
            {
                Debug.LogError("No Dice Gameobject found!");
                return;
            }
            BogDice temp_RefBogDice = null;
            string temp_strLetter = "";
            m_arrayGrid = new BogDice[m_iXsize, m_iYsize];
            int temp_iRotate = 0;
            for (int i = 0; i < m_iXsize; i++)
            {

                for (int j = 0; j < m_iYsize; j++)
                {
                    temp_RefBogDice = Instantiate(m_refGameObject, m_refDiceHolderParent);
                    m_arrayGrid[i, j] = temp_RefBogDice;

                    m_arrayGrid[i, j].transform.position += new Vector3(iXSpace * j, -iYSpace * i, 0);

                    int temp_iConList = (i + j) + i
;
                    //Pick a dice type and then randomly select one from there
                    // temp_strLetter = BogCommonHelper.const_arayCube4x4[i, j];//Random here too...
                    temp_strLetter = m_lstRandomDiceArray[(i + j) + i];
                    temp_strLetter = "" + temp_strLetter[RandomSeed(0, 6)];

                    int temp_iConvVal = (System.Convert.ToInt32(temp_strLetter[0]) - 65) + 1;

                   // Debug.Log("String Value" + temp_strLetter + ",value:" + temp_iConvVal + ",Anim" + "DiceRoll_" + (j + 1));
                    temp_RefBogDice._SpriteAttached.sprite = m_refBogResource.LstSprite[temp_iConvVal];
                    temp_RefBogDice.DiceValue = temp_strLetter;
                    temp_RefBogDice._DefaultSprite = m_refBogResource.LstSprite[temp_iConvVal];
                    temp_RefBogDice._AnimTriggerName = "DiceRoll_" + (j + 1);
                    m_lstBogDice.Add(m_arrayGrid[i, j]);
                    temp_RefBogDice.PlayRollDiceAnim(2.0f);
               
                  
                  
                }

            }
            //Some global anmation will come here....
            //and then call the events/.///
            StartCoroutine(coCallBackForBogBoardSetup());
        }


        IEnumerator coCallBackForBogBoardSetup()
        {
            Debug.LogError("CAalback called here");
            yield return new WaitForSeconds(2.2f);
            BogEventManager._Instance.Fire_evntOnUI_BogShuffleComplete();
        }
        public int RandomSeed(int a_iStartValue, int a_iEndValue)
        {
            return UnityEngine.Random.Range(a_iStartValue, a_iEndValue);
        }
        private void OnGameStateChangeLogic(EGameState a_NewState)
        {
          switch(a_NewState)
            {
                case EGameState.PlayingState:
                    ShowBoardDice();
                    break;
                case EGameState.ScoreDisplayState:
                    HideBoardDice();
                    break;
                case EGameState.ShuffleState:
                    ShowBoardDice();
                    break;
                case EGameState.StartRoundState:
                    HideBoardDice();
                    break;
            }
        }

        public void ShowBoardDice()
        {
            m_refDiceHolderParent.gameObject.SetActive(true);
        }
        public void HideBoardDice()
        {
            m_refDiceHolderParent.gameObject.SetActive(false);
        }
        #endregion BOG_BOARD_LOGIC


        #region Player_Interaction

        #endregion Player_Interaction
    }
}