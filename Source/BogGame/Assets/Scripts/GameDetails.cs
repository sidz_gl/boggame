﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    /// <summary>
    /// This holds the info required whenever a new Game session is started.
    /// Its easier this way maybe?
    /// </summary>
    [CreateAssetMenu(fileName = "GameDetails", menuName = "BogGame/GameDetails", order = 1)]
    public class GameDetails : ScriptableObject
    {

        public enum eMultiType
        {
            Host,
            Client
        }
        public enum eSessionType
        {
            Debug,
            Release,
        }

        [System.Serializable]
        public class SessionData
        {
            public BogCommonHelper.eGameType m_GameType = BogCommonHelper.eGameType.SinglePlayer;
            public string m_refIPHost="0.0.0.0";
            public eMultiType multiType = eMultiType.Host;
            public eSessionType eSessionType = eSessionType.Debug;
            public string roomName = "UNSET";
            public string port;
            public int gameSize = 1;
            public float gameTimer = 120;
                
           
        }

        [Header("Scene Transversal")]
        [SerializeField]private SessionData m_refCurrentSessionData;


        [Header("Game Scene Tools")]
        [SerializeField] private int m_fGameSeed;
        [SerializeField] private GameManager m_GameManager;
        [SerializeField] private BogTimer m_refBogTimer;
      

        [Header("Networking")]
        [SerializeField] private CustomLobbyManager m_refLobbyManager;
        [SerializeField] private Transform m_refCanvasRoot;

        [Header("Player Details")]
        [SerializeField] private PlayerScoreSet m_LocalPlayer;
        [SerializeField] private BogCommonHelper.eGameType m_GameType = BogCommonHelper.eGameType.SinglePlayer;
        [SerializeField] private List<PlayerScoreSet> m_lsOtherPlayers;


        public GameManager _GameManager
        {
            get
            {
                return m_GameManager;
            }

            set
            {
                m_GameManager = value;
            }
        }
        public int _GameSeed
        {
            get
            {
                return m_fGameSeed;
            }

            set
            {
                m_fGameSeed = value;
            }
        }
        public BogCommonHelper.eGameType _GameType
        {
            get
            {
                return m_GameType;
            }

            set
            {
                m_GameType = value;
            }
        }
        public PlayerScoreSet _LocalPlayer
        {
            get
            {
                return m_LocalPlayer;
            }

            set
            {
                m_LocalPlayer = value;
            }
        }
        public List<PlayerScoreSet> _LstOtherPlayers
        {
            get
            {
                return m_lsOtherPlayers;
            }

            set
            {
                m_lsOtherPlayers = value;
            }
        }

        public void CleanUpRound()
        {
            m_LocalPlayer = new PlayerScoreSet();
      
            _GameType = BogCommonHelper.eGameType.SinglePlayer;
            _LstOtherPlayers.Clear();
            Debug.Log("Game Details:CleanUpRound");
        }

        public void CreateNewRound()
        {
            CleanUpRound();
            m_LocalPlayer = new PlayerScoreSet();
            _LstOtherPlayers = new List<PlayerScoreSet>();
            Debug.Log("Game Details:CreateNewRound ");
        }
        public void ClearAllWordList()
        {
            m_LocalPlayer.ClearWords();
            foreach(PlayerScoreSet m_tempScoreSet in _LstOtherPlayers)
            {
                m_tempScoreSet.ClearWords();
            }
            Debug.Log("Game Details:Clearing Word list");
        }


        public void CreateSession(SessionData a_refCurrentSessionData)
        {
            m_refCurrentSessionData = a_refCurrentSessionData;
        }
        public void CloseSession()
        {
            m_refCurrentSessionData = null;
        }
        void OnDisable()
        {
            CloseSession();
        }

        #region NETWORKING
        public CustomLobbyManager _CustomLobbyManager
        {
            get
            {
                return m_refLobbyManager;
            }

            set
            {
                m_refLobbyManager = value;
            }
        }

        public Transform _CanvasRoot
        {
            get
            {
                return m_refCanvasRoot;
            }

            set
            {
                m_refCanvasRoot = value;
            }
        }

        public SessionData _CurrentSessionData
        {
            get
            {
                return m_refCurrentSessionData;
            }

            set
            {
                m_refCurrentSessionData = value;
            }
        }

        public BogTimer _BogTimer
        {
            get
            {
                return m_refBogTimer;
            }

            set
            {
                m_refBogTimer = value;
            }
        }

        public void AddOtherPlayer(PlayerScoreSet a_refOtherPlayer)
        {
            if (m_lsOtherPlayers.Contains(a_refOtherPlayer) == false)
            {
                m_lsOtherPlayers.Add(a_refOtherPlayer);
            }
            else
            {
                Debug.LogError("It already contains the player score set");
            }
        }
        #endregion NETWORKING

    }
}