﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Networking;
/// <summary>
/// A simple timer class with callbacks which fire when 
/// </summary>
/// 
namespace SIDZ.BogGame.Core
{
    public class BogTimer : MonoBehaviour
    {

        [SerializeField] private Animator m_refAnimator;
        [SerializeField] private Text m_UITimerText;
        [SerializeField] private GameDetails m_refGameDetails;

        [ContextMenu("Test ME")]
        public void TestMe_StartTimer()
        {
            StartTimer(m_fTimeInSeconds);
        }
        [ContextMenu("Stop ME")]
        public void TestMe_StopTimer()
        {
            StopTimer(true);
        }

        [SerializeField] private float m_fTimeInSeconds;
        void Start()
        {
            m_UITimerText.text = "";
            m_refGameDetails._BogTimer = this;
        }

        private Coroutine m_refTimerCoroutine = null;

        public void StartTimer(float a_fTimeToWait)
        {
            m_UITimerText.text = "";
            Debug.Log("StartTimer:for m_fTimeInSeconds ");
            if (m_refTimerCoroutine != null)
            {
                StopCoroutine(m_refTimerCoroutine);
                m_refTimerCoroutine = null;
                Debug.Log("StartTimer:A timer Already running Stopping it! ");
            }

            m_refTimerCoroutine = StartCoroutine(coTimer(a_fTimeToWait));
            _AttachedAnimator.SetTrigger("Trigger_StartTimer");
        }
        public void StopTimer(bool a_bIsForceStop)
        {
            if (m_refTimerCoroutine != null)
            {
                StopCoroutine(m_refTimerCoroutine);
                Debug.Log("StopTimer:Stopping a timer ! ");
                m_refTimerCoroutine = null;
                _AttachedAnimator.SetTrigger("Trigger_StopTimer");
                if (BogEventManager._Instance)
                {
                    BogEventManager._Instance.Fire_EventOnTimerOver(a_bIsForceStop);
                }
            }
            else
            {
                Debug.Log("StopTimer:No timer was  running ! ");
            }
        }


        private string temp_strDisplayTime = "";
        private float temp_fSeconds = 0;
        private float temp_fMinutes = 0;
        private string FormatTimerText(float a_fTimer)
        {
            temp_fSeconds = 0;
            temp_fMinutes = 0;
            if (a_fTimer > 60)
            {
                temp_fMinutes = Mathf.FloorToInt(a_fTimer / 60);
            }
            temp_fSeconds = Mathf.FloorToInt((a_fTimer % 60) * 1);
            temp_strDisplayTime = string.Format("{0:00}:{1:00}", temp_fMinutes, (temp_fSeconds));
            return temp_strDisplayTime;
        }
        private float temp_fT = 0;

        public Animator _AttachedAnimator
        {
            get
            {
                

                return m_refAnimator;
            }

            set
            {
                m_refAnimator = value;
            }
        }

        private IEnumerator coTimer(float a_fTimeToWait)
        {
            temp_fT = a_fTimeToWait;
            while (temp_fT >= 0)
            {
                temp_fT -= Time.deltaTime * 1.0f;
                m_UITimerText.text = FormatTimerText(temp_fT);
             
                yield return new WaitForEndOfFrame();
            }
            m_UITimerText.text = "00:00";
            StopTimer(false);
          
        }

        public void AddListener(BogEventManager.OnTimerOver a_CallBackonTimerOver)
        {
            BogEventManager._Instance.EventOnTimerOver += a_CallBackonTimerOver;
        }
        public void RemoveListener(BogEventManager.OnTimerOver a_CallBackonTimerOver)
        {
            //Not sure if its not there before
            BogEventManager._Instance.EventOnTimerOver -= a_CallBackonTimerOver;
        }

        public string GetCurrentTime()
        {
            return m_UITimerText.text;
        }
        public void SetCurrentTime(string a_strNewTimeValue)
        {
            m_UITimerText.text = a_strNewTimeValue;
        }
        /*
        #region NETWORK_SYNCDATA

        [Command]
        public void CmdNetSetTimerText(string a_strText)
        {
            if (m_refGameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host )
            {

                RpcNetSetTimerText(a_strText);
            }
        }

        [ClientRpc]
        public void RpcNetSetTimerText(string a_strText)
        {
            if (m_refGameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
            {
                Debug.LogError("All ready updated locally..");
                return;
            }

            if (_AttachedAnimator.GetCurrentAnimatorStateInfo(0).IsName("Trigger_StartTimer") != false)
            {
                _AttachedAnimator.SetTrigger("Trigger_StartTimer");
            }

            m_UITimerText.text = a_strText;
        }

        [Command]
        public void CmdNetStopAnimation()
        {
            RpcNetStopAnimation();
        }
        [ClientRpc]
        public void RpcNetStopAnimation()
        {
            if (m_refGameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
            {
                Debug.LogError("All ready updated locally..");

            }

            {
                _AttachedAnimator.SetTrigger("Trigger_StopTimer");
                m_UITimerText.text = "00:00";
            }
        }

        public string GetCurrentTime()
        {
            return m_UITimerText.text;
        }
        public void SetCurrentTime(string a_strNewTimeValue)
        {
             m_UITimerText.text = a_strNewTimeValue;
        }
        #endregion NETWORK_SYNCDATA
        */
    }
}