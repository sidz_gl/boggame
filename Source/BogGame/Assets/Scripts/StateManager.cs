﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    /*In Game State*/
    #region Game_State
    public abstract class BaseState : IStateSystem
    {
        [SerializeField] private List<IStateSystem> lstValidTransitionState;

        public virtual List<IStateSystem> ValidTransitionStates
        {
            get
            {
                return lstValidTransitionState;
            }

            set
            {
                lstValidTransitionState = value;
            }
        }
      

        public virtual void GoNextState()
        {

        }

        public  bool OnCanTransition(IStateSystem a_nxtState)
        {
            if(lstValidTransitionState == null || lstValidTransitionState.Count == 0)
            {
                Debug.Log("OnCanTransition:No lstValidTransitionState given...can go to any state");
                return true;
            }
            if(lstValidTransitionState.Contains(a_nxtState))
            {
                Debug.Log("OnCanTransition:This is a Valid State:"+ a_nxtState);
                return true;
            }
            Debug.Log("OnCanTransition:This is NOTT a Valid State:" + a_nxtState);
            return false;
        }

        public virtual void OnEnterState()
        {

        }

        public virtual void OnExitState()
        {

        }

        public virtual void OnTick()
        {

        }

        public  void SetValidTransitions(List<IStateSystem> a_ValidTransitionState)
        {
           if(lstValidTransitionState == null )
            {
                lstValidTransitionState = new List<IStateSystem>();
            }
            lstValidTransitionState.AddRange(a_ValidTransitionState);
            Debug.Log("SetValidTransitions is set!");
        }

        List<IStateSystem> IStateSystem.ValidTransitionStates()
        {
            return ValidTransitionStates;
        }
    }


    public class StartRoundState : BaseState
    {
        public override void GoNextState()
        {
            Debug.Log("GoNextState:StartRoundState");
        }

       

        public override void OnEnterState()
        {
            Debug.Log("OnEnterState:StartRoundState");
        }

        public override void OnExitState()
        {
            Debug.Log("OnExitState:StartRoundState");
        }

        public override void OnTick()
        {

        }
    }
    public class BogShuffleState : BaseState
    {
        public override void GoNextState()
        {
            Debug.Log("GoNextState:BogShuffleState");
        }
      

        public override void OnEnterState()
        {
            Debug.Log("OnEnterState:BogShuffleState");
        }

        public override void OnExitState()
        {
            Debug.Log("OnExitState:BogShuffleState");
        }

        public override void OnTick()
        {

        }
    }
    public class PlayingTimeState : BaseState
    {
        public override void GoNextState()
        {
            Debug.Log("GoNextState:PlayingTime");
        }
       

        public override void OnEnterState()
        {
            Debug.Log("OnEnterState:PlayingTime");
        }

        public override void OnExitState()
        {
            Debug.Log("OnExitState:PlayingTime");
        }

        public override void OnTick()
        {

        }
    }
    public class EndRoundState : BaseState
    {
        public override void GoNextState()
        {
            Debug.Log("GoNextState:EndRoundState");
        }

        public override void OnEnterState()
        {
            Debug.Log("OnEnterState:EndRoundState");
        }

        public override void OnExitState()
        {
            Debug.Log("OnExitState:EndRoundState");
        }

        public override void OnTick()
        {

        }
    }

    #endregion Game_State
    public class StateManager : MonoBehaviour
    {
        [SerializeField] private IStateSystem m_CurrentState;
        [SerializeField] private IStateSystem m_PreviousState;

        Dictionary<System.Type, IStateSystem> m_dictAllState;



        private void Start()
        {
            Init();
        }
        private void Update()
        {
            m_CurrentState.OnTick();
        }
        //Creates object or gives the one which is already there
        private IStateSystem GetState(System.Type a_Statetype)
        {
            if (m_dictAllState == null && m_dictAllState.ContainsKey(a_Statetype) == false)
            {
                return null;
            }
            return m_dictAllState[a_Statetype];
        }

        private void Init()
        {
            m_dictAllState = new Dictionary<System.Type, IStateSystem>();
            //Some cool ui later on...
            m_dictAllState.Add(typeof(StartRoundState), new StartRoundState());
            m_dictAllState.Add(typeof(BogShuffleState), new BogShuffleState());
            m_dictAllState.Add(typeof(PlayingTimeState), new PlayingTimeState());
            m_dictAllState.Add(typeof(EndRoundState), new EndRoundState());

            List<IStateSystem> temp_validTrans = new List<IStateSystem>();

            temp_validTrans.Clear();
            temp_validTrans.Add(m_dictAllState[typeof(BogShuffleState)]);
            m_dictAllState[typeof(StartRoundState)].SetValidTransitions(temp_validTrans);

            temp_validTrans.Clear();
            temp_validTrans.Add(m_dictAllState[typeof(PlayingTimeState)]);
            m_dictAllState[typeof(BogShuffleState)].SetValidTransitions(temp_validTrans);

            temp_validTrans.Clear();
            temp_validTrans.Add(m_dictAllState[typeof(EndRoundState)]);
            m_dictAllState[typeof(PlayingTimeState)].SetValidTransitions(temp_validTrans);

            temp_validTrans.Clear();
            temp_validTrans.Add(m_dictAllState[typeof(StartRoundState)]);
            m_dictAllState[typeof(EndRoundState)].SetValidTransitions(temp_validTrans);




            m_CurrentState = GetState(typeof(StartRoundState));
            m_PreviousState = GetState(typeof(StartRoundState));

          

        }

        private void OnGameStateChanged(IStateSystem a_NewState)
        {
            if(m_CurrentState.OnCanTransition(a_NewState))
            {
                m_CurrentState.GoNextState();
            }
        }

        public void NextState()
        {
            var nxtState = m_CurrentState.ValidTransitionStates()[0];
            m_PreviousState = m_CurrentState;
          
        }

    }
}