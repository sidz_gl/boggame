﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace SIDZ.BogGame.Core
{
    /// <summary>
    /// This creates words inside the list and removes them and all sorts of things...
    /// </summary>
    public class BogUIWordList : MonoBehaviour
    {
        [Header("Setttings:BogUIWordList")]
        [SerializeField] private Text m_refUITextPrefab;
        [SerializeField] private RectTransform m_UIcontentArea;
        [SerializeField] private List<Text> m_lstTextAdded;
        private Dictionary<string, Text> m_dictStrUIWord;

        [Header("Audio_LAzy to keep it seperate")]
        [SerializeField] private AudioSource m_refAudioSSource;
        [SerializeField] private AudioClip m_sfxCorrectWord;
        [SerializeField] private AudioClip m_sfxAlreadyPresent;


        //For controlling the scroll barss..
        private ScrollRect m_refScrollRect;
        private Scrollbar m_refHorizontalBar;
        private Scrollbar m_refVerticalBar;
        // Use this for initialization
        void Start()
        {
            Init();
        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnDisable()
        {
            if (BogEventManager._Instance)
            {
                BogEventManager._Instance.EvntOnUI_CorrectWord -= AddToWordList;
                BogEventManager._Instance.EvntOnUI_BogShuffle -= ClearWordList;
            }
        }
        bool IsValid()
        {
            return m_dictStrUIWord != null;
        }
        void Init()
        {
            m_dictStrUIWord = new Dictionary<string, Text>();

            m_refScrollRect = GetComponent<ScrollRect>();
            m_refHorizontalBar = m_refScrollRect.horizontalScrollbar;
            m_refVerticalBar = m_refScrollRect.verticalScrollbar;

            BogEventManager._Instance.EvntOnUI_BogShuffle += ClearWordList;
            BogEventManager._Instance.EvntOnUI_CorrectWord += AddToWordList;
        }

        /// <summary>
        /// Use this to disable user interaction
        /// </summary>
        /// <param name="a_bACtive"></param>
        public void SetScrollState(bool a_bACtive)
        {
            m_refHorizontalBar.interactable = a_bACtive;
            m_refVerticalBar.interactable = a_bACtive;
        }

        public void AddToWordList(string a_strWordToAdd)
        {
            if (IsValid() == false)
            {
                return;
            }
            Text temp_refText = null;
            if (CheckInWordList(a_strWordToAdd))
            {
                Debug.Log("BogUIWordList already contains this word" + a_strWordToAdd);//Maybe scroll to this point
                temp_refText = GetTextUIFromString(a_strWordToAdd);
                if (temp_refText != null)
                {
                    temp_refText.GetComponent<Animator>().SetTrigger("WrongWord");
                }
                m_refAudioSSource.clip = m_sfxAlreadyPresent;
                m_refAudioSSource.Play();
                return;
            }
            GameObject temp_textGO = Instantiate(m_refUITextPrefab.gameObject, m_UIcontentArea.transform);
            temp_refText = temp_textGO.GetComponent<Text>();
            temp_refText.text = a_strWordToAdd;
            temp_refText.transform.SetAsFirstSibling();
            m_dictStrUIWord.Add(a_strWordToAdd, temp_refText);
            m_refAudioSSource.clip = m_sfxCorrectWord;
            m_refAudioSSource.Play();

        }
        public void RemoveToWordList(string a_strWordToRemove)
        {
            if (IsValid() == false)
            {
                return;
            }
            if (m_dictStrUIWord.ContainsKey(a_strWordToRemove) == false)
            {
                Debug.Log("BogUIWordList doesnt contains this word" + a_strWordToRemove);//Maybe scroll to this point
                return;
            }
            Text temp_refText = m_dictStrUIWord[a_strWordToRemove];
            temp_refText.transform.SetParent(null);
            Destroy(temp_refText.gameObject);

        }
        public bool CheckInWordList(string a_strWordToCheck)
        {
            if (IsValid() == false)
            {
                return false;
            }
            if (m_dictStrUIWord.ContainsKey(a_strWordToCheck) == false)
            {
                Debug.Log("BogUIWordList doesnt contains this word" + a_strWordToCheck);//Maybe scroll to this point
                return false;
            }
            Text temp_refText = m_dictStrUIWord[a_strWordToCheck];
            temp_refText.transform.SetAsFirstSibling();
            return true;
        }
        private Text GetTextUIFromString(string a_strWordToCheck)
        {
            if (IsValid() == false)
            {
                Debug.LogError("GetTextUIFromString:BogUIWordList doesnt contains this word" + a_strWordToCheck);//Maybe scroll to this point

                return null;
            }
            if (m_dictStrUIWord.ContainsKey(a_strWordToCheck) == false)
            {
                Debug.LogError("GetTextUIFromString:BogUIWordList doesnt contains this word" + a_strWordToCheck);//Maybe scroll to this point
                return null;
            }
            return m_dictStrUIWord[a_strWordToCheck];

        }

        public void ClearWordList()
        {
            if (IsValid() == false)
            {
                return;
            }
            foreach (string strKey in m_dictStrUIWord.Keys)
            {
                m_dictStrUIWord[strKey].transform.SetParent(null);
                Destroy(m_dictStrUIWord[strKey].gameObject);
            }
            m_dictStrUIWord.Clear();
        }

        public List<string> GetAllWord()
        {
            if (IsValid() == false)
            {
                return null;
            }

            List<string> temp_lstWordAdd = new List<string>();
            foreach (string words in m_dictStrUIWord.Keys)
            {
                temp_lstWordAdd.Add(words);
            }
            return temp_lstWordAdd;
        }

    }
}