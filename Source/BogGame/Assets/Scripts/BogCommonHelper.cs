﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    

    public interface IStateSystem
    {
        void GoNextState();
        void OnEnterState();
        void OnExitState();
        void OnTick();
        bool OnCanTransition(IStateSystem a_nxtState);
        void SetValidTransitions(List<IStateSystem> a_ValidTransitionState);
        List<IStateSystem> ValidTransitionStates();
    }
   



    /// <summary>
    /// A General class representing a  player game object...
    /// </summary>
    public class PlayerInfo
    {
        public string a_strName = "John Doe";
        public int a_iScore = 0;
    }

    interface IStateObject
    {
        bool IsActiveState();
        BogCommonHelper.eActiveState GetCurrentState();
        void SetActiveState(BogCommonHelper.eActiveState a_eCurrentActiveState);
    }
    public class BogCommonHelper
    {
        public static readonly string const_strDictFilePath = "E:\\HobbyProjects\\BogGame\\BogGame\\REsource\\wordlst.txt";

        public static readonly string[,] const_arayCube4x4 = new string[4, 4]
        {
            { "AAEEGN", "ABBJOO", "ACHOPS", "AFFKPS" },
            { "AOOTTW", "CIMOTU", "DEILRX", "DELRVY" },
            { "DISTTY", "EEGHNW", "EEINSU", "EHRTVW" },
            { "EIOSST", "ELRTTY", "HIMNQU", "HLNNRZ" },
         };

        /// <summary>
        /// A Global enum to disable all the actives of any module...
        /// </summary>
        public enum eActiveState
        {
            Enabled,
            Disabled
        }
        public enum eGameType
        {
            SinglePlayer,
            MultiPlayer
        }


        public static class IPManager
        {
            public static string GetLocalIPAddress()
            {
                var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }

                throw new System.Exception("No network adapters with an IPv4 address in the system!");
            }
        }


    }


}