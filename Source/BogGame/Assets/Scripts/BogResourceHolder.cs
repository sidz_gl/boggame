﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    //TODO:Move this to a Base class Please!!!!
    [CreateAssetMenu(fileName = "BogResourceHolder", menuName = "BogGame/BogResourceHolder", order = 1)]
    public class BogResourceHolder : ScriptableObject
    {
        [Header("Art:Letter")]
        [SerializeField] private List<Sprite> m_lstSprite;

        [Header("Word List:BogWordValidator")]
       
        [SerializeField] private List<string> m_strSupportedWords;

        // Use this for initialization
        public List<Sprite> LstSprite
        {
            get
            {
                return m_lstSprite;
            }


        }


        #region WORD_DICT
        private void LoadWordFromTextFile(string a_strFilePath)
        {
            m_strSupportedWords = new List<string>();

            using (StreamReader sr = File.OpenText(a_strFilePath))
            {
                string s = string.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    //do your stuff here
                    m_strSupportedWords.Add(s);
                }
            }
            Debug.Log("Add Word Count:"+ m_strSupportedWords.Count);
        }

        [ContextMenu("Update Word list")]
        public void UpdateWordList()
        {
            LoadWordFromTextFile(BogCommonHelper.const_strDictFilePath);
        }
        public virtual bool ValidWord(string m_aWord)
        {
            return (m_strSupportedWords!=null && m_strSupportedWords.Count>0&&
                   m_strSupportedWords.Contains(m_aWord));
        }
        #endregion WORD_DICT

     
    }
}