﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BogAudioManager : MonoBehaviour
{
    private static BogAudioManager _instance;
    private BogAudioManager()
    {
    }

    public static BogAudioManager _Instance
    {
        get
        {

            if (_instance == null)
            {
                _instance = FindObjectOfType<BogAudioManager>();
            }
            return _instance;
        }

    }



    void Awake()
    {
        //Check if instance already exists
        if (_instance == null)

            //if not, set instance to this
            _instance = this;

        //If instance already exists and it's not this:
        else if (_instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    #region LOGIC_HERE
    [Header("Settings For Audio")]
    [SerializeField] private AudioSource m_refBGMusic;
    [SerializeField] private AudioSource m_refFXSound;

    [SerializeField] private List<AudioClip> m_lstMusic;
    [SerializeField] private List<AudioClip> m_lstSFX;

    public void PlaySFXSound(int a_iID)
    {
        m_refFXSound.clip = m_lstSFX[a_iID];
        m_refFXSound.Play();
    }
    public void PlayMusic(int a_iID,bool m_bPlayAndLoop)
    {
        m_refBGMusic.Stop();
        m_refBGMusic.clip = m_lstMusic[a_iID];
        m_refBGMusic.Play();
    }
    public void StopMusic()
    {

    }
    public void PlayFXSound(int a_iID, bool m_bPlayAndLoop)
    {

    }
    public void StopPlayFXSound()
    {

    }
    public void SetMuteAudioListener(bool a_bState)
    {

        AudioListener.volume = a_bState ? 1.0f : 0.0f;
       
    }


    #endregion LOGIC_HERE
}
