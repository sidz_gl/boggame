﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SIDZ.BogGame.Core
{
    //A start from 65 + 
    /*
    const string[] kStandardCubes = []{
"AAEEGN", "ABBJOO", "ACHOPS", "AFFKPS",
"AOOTTW", "CIMOTU", "DEILRX", "DELRVY",
"DISTTY", "EEGHNW", "EEINSU", "EHRTVW",
"EIOSST", "ELRTTY", "HIMNQU", "HLNNRZ"
};
*/


    public class BogDice : MonoBehaviour
    {
        public enum eDiceState
        {
            eUnSelected,
            eSelected,
        }

        [SerializeField] private string m_strDiceValue = "";

        [SerializeField] private AudioClip m_sfxSelectedSound;
        [SerializeField] private AudioClip m_sfxUnSelectedSound;


        private List<Sprite> m_lstFaceSprite;
        private SpriteRenderer m_refSprite;

        private Animator m_refAnimator;
        private string m_strAnimTriggerName = "DiceRoll_1";
        private Sprite m_refDefSprite = null;

        private eDiceState m_eCurrentDiceState = eDiceState.eUnSelected;

        private Collider m_refAttachedCollider = null;
        private AudioSource m_refAudioSource;

        public string DiceValue
        {
            get
            {
                return m_strDiceValue;
            }

            set
            {
                m_strDiceValue = value;
            }
        }

        public SpriteRenderer _SpriteAttached
        {
            get
            {
                if (m_refSprite == null)
                {
                    _SpriteAttached = GetComponent<SpriteRenderer>();
                }
                return m_refSprite;
            }

            set
            {
                m_refSprite = value;
            }
        }

        public Animator _Animator
        {
            get
            {
                if (m_refAnimator == null)
                {
                    m_refAnimator = GetComponent<Animator>();
                }
                return m_refAnimator;
            }

            set
            {
                m_refAnimator = value;
            }
        }

        public string _AnimTriggerName
        {
            get
            {
                return m_strAnimTriggerName;
            }

            set
            {
                m_strAnimTriggerName = value;
            }
        }

        public Sprite _DefaultSprite
        {
            get
            {
                return m_refDefSprite;
            }

            set
            {
                m_refDefSprite = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            m_refAudioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        #region BOG_DICE


        private void Init()
        {
            _SpriteAttached = GetComponent<SpriteRenderer>();
            m_refAttachedCollider = GetComponent<Collider>();
            _Animator = GetComponent<Animator>();

        }

        public void PlayRollDiceAnim(float a_strRollTime)
        {

            if (_Animator == null)
            {
                Debug.LogError("No Animator Attached");
                return;
            }

            StartCoroutine(coStartRollingAnim(a_strRollTime));

        }
        [ContextMenu("Test Def")]
        public void ResetDiceToDefault()
        {
            _Animator.enabled = false;
            _SpriteAttached.sprite = _DefaultSprite;
           //  Debug.LogError(" !!!Change_SpriteAttached.sprite" + _SpriteAttached.sprite.name);

        }
        private void PlayRollDice()
        {
            _Animator.enabled = true;
            _Animator.SetTrigger(_AnimTriggerName);
        }
        private void StopRollDice()
        {
            // _Animator.SetTrigger("Dice_Idle");
            _Animator.Play("Idle");
            //A Bad way to do this
            Debug.Log("Stopping Roll Dice");



        }
        private IEnumerator coStartRollingAnim(float a_strRollTime)
        {
            PlayRollDice();
            yield return new WaitForSeconds(a_strRollTime);
            StopRollDice();

        }
        public void ChangeDiceState(eDiceState a_eDiceState)
        {
            if (m_eCurrentDiceState == a_eDiceState)
            {
                Debug.LogError("Changing Dice To Same State." + a_eDiceState);
                return;
            }


            m_eCurrentDiceState = a_eDiceState;
            ChangeDiceStateLogic(a_eDiceState);

        }
        private void ChangeDiceStateLogic(eDiceState a_eDiceState)
        {
            switch (a_eDiceState)
            {
                case eDiceState.eSelected:
                  
                    transform.localScale *= 0.85f;
                    // Dice_Selected
        
                    _SpriteAttached.sprite = _DefaultSprite;

                    _SpriteAttached.color = new Color(0,1,0,1);
                    m_refAudioSource.clip = m_sfxSelectedSound;
                    m_refAudioSource.Play();
                    break;
                case eDiceState.eUnSelected:
                    transform.localScale *= (1.0f / 0.85f);
                    _SpriteAttached.color = new Color(1, 1, 1, 1);
                    m_refAudioSource.clip = m_sfxUnSelectedSound;
                    m_refAudioSource.Play();

                    break;
            }
        }

     

        #endregion BOG_DICE
    }
}