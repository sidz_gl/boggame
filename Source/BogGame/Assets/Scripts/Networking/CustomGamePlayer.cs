﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
namespace SIDZ.BogGame.Core
{
    public class CustomGamePlayer : NetworkBehaviour
    {
        [Header("Settings:CustomLobbyManager")]
        [SerializeField] private GameDetails m_GameDetails;
        [SyncVar]
        public bool m_bIsHostPlayer = false;

        [SyncVar]
        public string m_strTime;


        private void Start()
        {

            Init();
        }
        private void Init()
        {
            m_bIsHostPlayer = isServer && isLocalPlayer;
            BogEventManager._Instance.EvntOnUI_CorrectWord += AddWordToGameDetails;
            BogEventManager._Instance.EvntOnGameStateChange += OnGameStateChangedCalled;
            if (isLocalPlayer)
            {
                this.name += m_GameDetails.name;
            }

        }
        void OnDisable()
        {
            BogEventManager._Instance.EvntOnUI_CorrectWord -= AddWordToGameDetails;
            BogEventManager._Instance.EvntOnGameStateChange -= OnGameStateChangedCalled;
        }

        private void OnGameStateChangedCalled(EGameState a_NewState)
        {
            Debug.Log("GamePlayer:OnGameStateChangedCalled" + a_NewState);
            SyncStateToOtherGames(a_NewState);

            switch (a_NewState)
            {
                case EGameState.ScoreDisplayState:
                    Debug.Log("OnGameStateChangedCalled:Going To Send Details to others.." + EGameState.ScoreDisplayState);
                    if (isLocalPlayer)
                    {
                        CmdSendScoreDetails(m_GameDetails._LocalPlayer.m_strName, m_GameDetails._LocalPlayer.m_lstString.ToArray(),
                            m_GameDetails._LocalPlayer.m_strNetID);
                        Debug.Log("OnGameStateChangedCalled:Going To Send Details to others ,Master will send me values...");

                    }
                    else
                    {
                        Debug.Log("OnGameStateChangedCalled:Going To Send Details to others But this is not local player,,wil wait for my Master");
                    }

                    if (m_refUpdateTimerForOthers != null)
                    {
                        StopCoroutine(m_refUpdateTimerForOthers);
                    }
                    CmdNetStopAnimation();
                    break;
                case EGameState.PlayingState:
                    if (m_GameDetails._CurrentSessionData.multiType != GameDetails.eMultiType.Host)
                    {
                        Debug.LogError("Will wait for host to update");
                        return;
                    }
                    if(m_refUpdateTimerForOthers!=null)
                    {
                        StopCoroutine(m_refUpdateTimerForOthers);
                    }
                    m_refUpdateTimerForOthers = StartCoroutine(coKeepTimerInSync());
                    break;
               
                 



            }


            //If we are in score stage send out details to other people
        }



        private void SyncStateToOtherGames(EGameState a_NewState)
        {
            //This is for sending the NEW STATE ACROSS 
            if (isServer && isLocalPlayer)
            {
                CmdChangeStateForPlayers(a_NewState);
            }
            else
            {
                Debug.Log("GamePlayer:OnGameStateChangedCalled Failed bcos of (isServer && isLocalPlayer)" + a_NewState);
            }
        }


        private void AddWordToGameDetails(string a_strWord)
        {
            Debug.Log("Bog Pointer:Correct Word adding to Game Details of Local player:" + a_strWord);
            m_GameDetails._LocalPlayer.AddWord(a_strWord);

        }

        //listen to event change and fire cmd and rpc from here
        #region CALLS
        [Command]
        public void CmdChangeStateForPlayers(EGameState a_NewState)
        {
            if (isServer && isLocalPlayer)
            {
                RpcChangeStateForPlayers(a_NewState);
                Debug.Log("GamePlayer:CmdChangeStateForPlayers called:New State:" + a_NewState);
            }
            else
            {
                Debug.LogError("GamePlayer:CmdChangeStateForPlayers Failed bcos of (isServer && isLocalPlayer)" + a_NewState);
            }
        }
        [ClientRpc]
        public void RpcChangeStateForPlayers(EGameState a_NewState)
        {
            if (!(isServer && isLocalPlayer))
            {
                m_GameDetails._GameManager.ChangeToState(a_NewState);
                Debug.Log("GamePlayer:RpcChangeStateForPlayers called:New State:" + a_NewState);
            }
            else
            {
                Debug.LogError("GamePlayer:RpcChangeStateForPlayers Failed bcos of (isServer && isLocalPlayer)" + a_NewState);
            }
        }

        //Listen to events
        //ON display score send you values to other pepople
        //Get ur local string first
        //call cmd 
        //Rpc non local player check in game details for name and update scroll list

        [Command]
        void CmdSendScoreDetails(string a_strPlayerName, string[] a_refPlayerScoreSet, string a_strNetConnectionID)
        {
            ///*connectionToClient.connectionId*/
            RpcSendScoreDetails(a_strPlayerName, a_refPlayerScoreSet, a_strNetConnectionID);
        }
        [ClientRpc]
        void RpcSendScoreDetails(string a_strPlayerName, string[] a_refPlayerScoreSet, string a_strNetConnectionID)
        {
            //NOTE:The other client gives out every cmd even of non local player hence it gets called here tooo!
            if (isLocalPlayer)
            {
                Debug.Log("RpcSendScoreDetails:No Need to Update for an LOcal Player" + this.name);
                return;
            }
            Debug.Log("RpcSendScoreDetails:Recevid data of string from!!!" + a_strPlayerName);
            List<string> temp = new List<string>(a_refPlayerScoreSet);
            for (int i = 0; i < m_GameDetails._LstOtherPlayers.Count; i++)
            {
                if (m_GameDetails._LstOtherPlayers[i].m_strNetID == a_strNetConnectionID)
                {
                    Debug.Log("Player Found In Other Player List at:" + i);
                    m_GameDetails._LstOtherPlayers[i].AddPlayerWordList(temp);
                    // BogEventManager._Instance.Fire_EvntOnOtherPlayerDataRcvd(a_strPlayerName, a_iConnID + "");
                    break; ;
                }
            }
            BogEventManager._Instance.Fire_EvntOnOtherPlayerDataRcvd(a_strPlayerName, a_strNetConnectionID + "");

        }

        //Talk with the game manager and update time text
        #endregion CALLS


        #region NETWORK_SYNCDATA
        private Coroutine m_refUpdateTimerForOthers = null;
        IEnumerator coKeepTimerInSync()
        {
            while(true)
            {
                CmdNetSetTimerText();
                yield return new WaitForEndOfFrame();
            }
            
        }

        [Command]
        public void CmdNetSetTimerText()
        {
            if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
            {
                RpcNetSetTimerText(m_GameDetails._BogTimer.GetCurrentTime());
            }
        }

        [ClientRpc]
        public void RpcNetSetTimerText(string a_strText)
        {
            if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
            {
                Debug.LogError("All ready updated locally..");
                return;
            }
            /*
            if (m_GameDetails._BogTimer._AttachedAnimator.GetCurrentAnimatorStateInfo(0).IsName("Trigger_StartTimer") != false)
            {
                m_GameDetails._BogTimer._AttachedAnimator.SetTrigger("Trigger_StartTimer");
            }*/


            m_GameDetails._BogTimer.SetCurrentTime(a_strText);
        }

        [Command]
        public void CmdNetStopAnimation()
        {
            RpcNetStopAnimation();
        }
        [ClientRpc]
        public void RpcNetStopAnimation()
        {
            if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
            {
                Debug.LogError("All ready updated locally..");

            }

            {
                m_GameDetails._BogTimer._AttachedAnimator.SetTrigger("Trigger_StopTimer");
                m_GameDetails._BogTimer.SetCurrentTime("00:00");
            }
        }
        #endregion NETWORK_SYNCDATA


    }
}