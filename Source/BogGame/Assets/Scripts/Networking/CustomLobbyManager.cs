﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
namespace SIDZ.BogGame.Core
{
    public class CustomLobbyManager : NetworkLobbyManager
    {

        [Header("Settings:UI")]
        [SerializeField] private Button m_refHomeButton;
        [SerializeField] private GameObject m_refLocalInfo;
        [SerializeField] private Text m_UITextIP;

        [Header("Settings:CustomLobbyManager")]
        [SerializeField] private GameDetails m_GameDetails;
        [SerializeField] private Transform m_RootCanvas;
        [SerializeField] private CustomNetworkDiscovery customNetworkDiscovery;

        [SerializeField] private Button m_refStartGameButton;


        private void Start()
        {
            Cursor.visible = true;
            Init();
        }
        void Init()
        {


            SetupGame();
        }

        private void TogglePlayerInfo(bool a_bNewState)
        {
            m_UITextIP.text = networkAddress;
            m_UITextIP.text =    "Name:"+m_GameDetails._CurrentSessionData.roomName+"\nIP:"+BogCommonHelper.IPManager.GetLocalIPAddress();
            m_refLocalInfo.SetActive(a_bNewState);
        }
        public void SetupGame()
        {
            Debug.Log("CustomManagerName:Setup Game Called!");
            TogglePlayerInfo(true);

            // m_GameDetails.CleanUpRound();
            m_GameDetails._CustomLobbyManager = this;
            m_GameDetails._CanvasRoot = m_RootCanvas;
            m_refStartGameButton.gameObject.SetActive(false);

            //Check game details on the type of build it is in
            //If debug do nothings
            //Else start host join set size etc...
            if (m_GameDetails._CurrentSessionData == null)
            {
                Debug.LogError("Current Session Data is Null ,so doing nothing...");
                return;
            }

            switch (m_GameDetails._CurrentSessionData.eSessionType)
            {
                case GameDetails.eSessionType.Debug:
                    Debug.LogError("Current Session Data is Debug ,so doing nothing...");



                    break;
                case GameDetails.eSessionType.Release:
                    this.showLobbyGUI = false;
                    if (m_GameDetails._CurrentSessionData.m_GameType == BogCommonHelper.eGameType.SinglePlayer)
                    {
                        Debug.LogError("Current Session Data set for SinglePlayer!.");
                        this.networkAddress = m_GameDetails._CurrentSessionData.m_refIPHost;
                        minPlayers = 1;
                        maxPlayers = 1;
                        maxPlayersPerConnection = 1;
                        StartHost();
                        customNetworkDiscovery.StartBroadcastAsServer(m_GameDetails._CurrentSessionData.roomName, m_GameDetails._CurrentSessionData.m_refIPHost, m_GameDetails._CurrentSessionData.port);
                        // m_GameDetails._CurrentSessionData.m_refIPHost
                    }
                    else
                    {
                        Debug.LogError("Current Session Data set for Multiplayer..!.");
                        if (m_GameDetails._CurrentSessionData.multiType == GameDetails.eMultiType.Host)
                        {
                            Debug.LogError("Current Session Data set for Multiplayer  as HOSTING..!.");
                            this.networkAddress = m_GameDetails._CurrentSessionData.m_refIPHost;
                            minPlayers = m_GameDetails._CurrentSessionData.gameSize;//Net to add an UI for size
                            maxPlayers = m_GameDetails._CurrentSessionData.gameSize;
                            maxPlayersPerConnection = 1;
                            StartHost();
                            customNetworkDiscovery.StartBroadcastAsServer(m_GameDetails._CurrentSessionData.roomName, m_GameDetails._CurrentSessionData.m_refIPHost, m_GameDetails._CurrentSessionData.port);
                        }
                        else
                        {
                            Debug.LogError("Current Session Data set for Multiplayer  as Client..!.");
                            this.networkAddress = m_GameDetails._CurrentSessionData.m_refIPHost;
                            StartClient();
                            customNetworkDiscovery.StartBroadcastAsClient();

                        }
                    }

                    break;

            }

        }
        public override void OnLobbyStartServer()
        {
            customNetworkDiscovery.StartBroadcastAsServer(m_GameDetails._CurrentSessionData.roomName, m_GameDetails._CurrentSessionData.m_refIPHost, m_GameDetails._CurrentSessionData.port);
            m_GameDetails.CreateNewRound();
            m_refStartGameButton.gameObject.SetActive(true);
            m_refStartGameButton.interactable = false;
            if (m_GameDetails._CurrentSessionData.eSessionType == GameDetails.eSessionType.Debug)
            {
                m_GameDetails._CurrentSessionData.multiType = GameDetails.eMultiType.Host;
                Debug.LogError("Setting Game Details as HOST");
            }


            m_GameDetails._GameSeed = 52526;//--->
            base.OnLobbyStartServer();

        }

        public override void OnLobbyStartClient(NetworkClient lobbyClient)
        {

            m_GameDetails.CreateNewRound();
            base.OnLobbyStartClient(lobbyClient);
            if (m_GameDetails._CurrentSessionData.eSessionType == GameDetails.eSessionType.Debug && !NetworkServer.active)
            {
                m_GameDetails._CurrentSessionData.multiType = GameDetails.eMultiType.Client;
                Debug.LogError("Setting Game Details as Client");
            }
        }
        public void AllPlayersReadyDone()
        {

            ServerChangeScene(playScene);
            Debug.Log("AllPlayersReadyDone:All players will be taken in");
            m_refStartGameButton.gameObject.SetActive(false);
            m_refHomeButton.gameObject.SetActive(false);
            TogglePlayerInfo(false);
        }

        public override void OnLobbyClientEnter()
        {
            base.OnLobbyClientEnter();
            m_refStartGameButton.gameObject.SetActive(true);

            m_refHomeButton.gameObject.SetActive(true);

        }
        public override void OnLobbyClientExit()
        {
            base.OnLobbyClientExit();
            m_refStartGameButton.gameObject.SetActive(false);
        }

        public override void OnLobbyServerPlayersReady()
        {
            //base.OnLobbyServerPlayersReady();
            m_refStartGameButton.interactable = true;
            Debug.LogError("All players are in,Please Press Start:(call AllPlayersReadyDone())!!!");
        }
        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);
            Debug.LogError("A Client Conneted" + conn.address);
            if (m_GameDetails._CurrentSessionData != null && m_GameDetails._CurrentSessionData.multiType != GameDetails.eMultiType.Host)
            {
                Debug.LogError("A Client Conneted is not Host,disable start button" + conn.address);
                m_refStartGameButton.gameObject.SetActive(false);
            }
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            base.OnClientDisconnect(conn);
            Debug.LogError("A Client disconned" + conn.address);
            CloseNetworkManager();
        }

        public void StopGame()
        {
            StopClient();
            StopHost();
            m_refHomeButton.gameObject.SetActive(false);
            StartCoroutine(coDelayedNetShutDown());
        }

        public void CloseNetworkManager()
        {
            Debug.LogError("CloseNetworkManager Called");
            StopClient();
            StopHost();
            StartCoroutine(coDelayedNetShutDown());
        }



        IEnumerator coDelayedNetShutDown()
        {
            yield return new WaitForSeconds(2);
            Destroy(this.gameObject);
            NetworkManager.Shutdown();
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }


    }




}