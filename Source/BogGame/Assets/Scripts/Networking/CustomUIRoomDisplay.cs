﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace SIDZ.BogGame.Core
{
    public class CustomUIRoomDisplay : MonoBehaviour
    {

        [Header("Settings:CustomUIRoomDisplay")]
        [SerializeField] private InputField m_refJoinRoomText;
        [SerializeField] private ScrollRect m_UIscrollRoomAvaible;
        [SerializeField] private Button m_UIRoomText;
       


        List<string> m_lstRoomIp;
        List<Button> m_lstButtons;
        //Hold the list of games 
        //with their buttons
        //Button will call back this class
        //voila we have everything...

        //Add player found here and so on and forth

        private void Start()
        {
            Init();
        }

        void Init()
        {
            m_lstRoomIp = new List<string>();
            m_lstButtons = new List<Button>();
            BogEventManager._Instance.EvntOnRoomFound += AddPlayer;

        }
        void OnDisable()
        {
            if(BogEventManager._Instance)
            BogEventManager._Instance.EvntOnRoomFound -= AddPlayer;
        }
        [ContextMenu("Test Me")]
        public void TestMe()
        {
            AddPlayer("Super Man","0.0.0.0","1000");
        }

        public void AddPlayer(string a_strRoomName, string a_strRoomIP, string a_strRoomPort)
        {
            if (m_lstRoomIp.Contains(a_strRoomIP) == false)
            {
                m_lstRoomIp.Add(a_strRoomIP);
            }
            else
            {
                Debug.LogError("Room already present:"+ a_strRoomName);
                return;
            }
            Button tempButton = Instantiate(m_UIRoomText, m_UIscrollRoomAvaible.content);
            tempButton.onClick.AddListener(delegate { BtnRoomSelected(tempButton); });
            tempButton.transform.GetChild(0).GetComponent<Text>().text = a_strRoomName;
            if (m_lstButtons.Contains(tempButton) == false)
            {
                m_lstButtons.Add(tempButton);
            }
        }
        public void RemovePlayer()
        {

        }


        public void BtnRoomSelected(Button m_refSelfButton)
        {
            if(m_lstButtons.Contains(m_refSelfButton))
            {
               int i= m_lstButtons.IndexOf(m_refSelfButton);
                m_refJoinRoomText.text = m_lstRoomIp[i];
            }
        }

    }
}