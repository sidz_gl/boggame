﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
namespace SIDZ.BogGame.Core
{
    [System.Serializable]
    public class BogNetwormMsg
    {
        public string m_strData;
        public string m_strIP;
        public string m_strPort;
        public string m_RoomName = "NotSet";

      

        public BogNetwormMsg(string a_strIPAdress, string a_strData)
        {
            m_strIP = a_strIPAdress.Substring(a_strIPAdress.LastIndexOf(":") + 1, a_strIPAdress.Length - (a_strIPAdress.LastIndexOf(":") + 1));
            m_strData = a_strData;
            m_RoomName = a_strData;
        }
    }
    public class CustomNetworkDiscovery : NetworkDiscovery
    {
        [SerializeField] private List<BogNetwormMsg> m_lstCurrentRoom;


        private Coroutine m_TimeOut;
        void Awake()
        {
            Initialize();
        }

        [ContextMenu("StartBroadcastAsClient")]
        public void StartBroadcastAsClient()
        {
            if (running == false)
            {
                Initialize();
            }
            if (isServer)
                StopBroadcast();


            StartAsClient();
         //   m_TimeOut = StartCoroutine(coTimeOut());
        }

       
        IEnumerator coTimeOut()
        {
            while (true)
            {
                yield return new WaitForSeconds(10);
                Debug.LogWarning("Clear Room list...");
            }
        }

        [ContextMenu("StartBroadcastAsServer")]
        public void StartBroadcastAsServer(string a_strRoomName, string a_strRoomIP, string a_strRoomPort)
        {
            if(running)
            {
                Debug.LogWarning("Already running,not starting :StartBroadcastAsServer() called");
                return;
            }
         //   StopBroadcast();
            Initialize();
            broadcastData = a_strRoomName;
            StartAsServer();
        }

        public void CustomStopBroadcast()
        {
            if (running)
             StopBroadcast();

            if(m_TimeOut !=null)
            {
                StopCoroutine(m_TimeOut);
                m_TimeOut = null;
            }
        }
        public override void OnReceivedBroadcast(string fromAddress, string data)
        {
            base.OnReceivedBroadcast(fromAddress, data);

            if (m_lstCurrentRoom == null)
            {
                m_lstCurrentRoom = new List<BogNetwormMsg>();
            }
            bool m_bShoudAdd = true;
            BogNetwormMsg temp = new BogNetwormMsg(fromAddress, data);
            for (int i = 0; i < m_lstCurrentRoom.Count; i++)
            {
                if (m_lstCurrentRoom[i].m_strIP == temp.m_strIP)
                {
                    m_bShoudAdd = false;
                    Debug.Log("OnReceivedBroadcast:Already added in the list");
                    return;
                }
            }


            if (m_bShoudAdd)
            {
                m_lstCurrentRoom.Add(temp);
                BogEventManager._Instance.Fire_EvntOnRoomFound(temp.m_RoomName, temp.m_strIP, temp.m_strPort);
                Debug.Log("OnReceivedBroadcastAdded in the list");
            }
        }

    }
}