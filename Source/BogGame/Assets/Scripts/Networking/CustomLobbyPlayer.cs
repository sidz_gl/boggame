﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace SIDZ.BogGame.Core
{
    public class CustomLobbyPlayer : NetworkLobbyPlayer
    {
        [Header("Settings:CustomLobbyManager")]
        [SerializeField] private GameDetails m_GameDetails;

        [SerializeField] private InputField m_refNameInputField;
        [SerializeField] private Button m_refNameButton;
        [SerializeField] private Image m_refUINetInfo;

        [Header("Player Details")]
        [SerializeField] private PlayerScoreSet m_myPlayerScoreSet;

        [SyncVar]
        public bool m_bIsHostPlayer = false;

        public delegate void evntPlayerReady(string a_netID,string a_strName,string a_strIPAdress);


        [SyncVar] public string m_strPlayerName;

        [SyncEvent]
        public event evntPlayerReady EventPlayerReadyFromServer;


        public void Start()
        {
            Init();

        }
        public override void OnStartLocalPlayer()
        {
            ToggleDisplaySet(true);
            base.OnStartLocalPlayer();
        }

        private void Init()
        {
            m_bIsHostPlayer = isServer && isLocalPlayer;
            if (NetworkClient.active)
            {
                EventPlayerReadyFromServer += callbk_PlayersReady;
            }
            transform.SetParent(m_GameDetails._CanvasRoot, false);

            //So it will be always at top
            if (isLocalPlayer)
            {
                transform.SetAsFirstSibling();
                m_refUINetInfo.color =Color.green;

            }
            else
            {
                m_refUINetInfo.color = Color.black;
            }

            m_myPlayerScoreSet = new PlayerScoreSet();
            m_refNameInputField.interactable = false;
            m_refNameButton.interactable = false;

            ToggleDisplaySet(isLocalPlayer);
            ToggleDisplaySet(isLocalPlayer);

        
                Debug.Log("CmdUpdateGameDetailsFromServer:Asking for an update..."+name+""+isLocalPlayer);
            //if(isLocalPlayer)
                CmdUpdateGameDetailsFromServer();
               


        }
        /// <summary>
        /// Based on whether is local and non local player...
        /// </summary>
        /// <param name="a_bDisplay"></param>
        private void ToggleDisplaySet(bool a_bDisplay)
        {
            //  m_refNameInputField.gameObject.SetActive(a_bDisplay);
            m_refNameInputField.interactable = a_bDisplay;

            m_refNameButton.gameObject.SetActive(a_bDisplay);
            m_refNameButton.interactable = a_bDisplay;
        }

        private void callbk_PlayersReady(string a_netID, string a_strName, string a_strIPAdress)
        {

            Debug.Log("CustomLobbyPlayer:Callbk_PlayersReady REceived...." + a_strName + ",,islocalPlayer:" + isLocalPlayer);
            m_strPlayerName = a_strName;
            if (isLocalPlayer)
            {
                m_refNameInputField.text = a_strName;
                m_myPlayerScoreSet.m_strName = a_strName;
                m_GameDetails._LocalPlayer = m_myPlayerScoreSet;
                m_GameDetails._LocalPlayer.m_strNetID = a_netID;
                m_GameDetails._LocalPlayer.IP_Address = a_strIPAdress;
            }
            else
            {
                m_refNameInputField.text = a_strName;
                m_myPlayerScoreSet.m_strName = a_strName;
                m_myPlayerScoreSet.m_strNetID = a_netID;
                m_GameDetails._LocalPlayer.IP_Address = a_strIPAdress;
                m_GameDetails.AddOtherPlayer(m_myPlayerScoreSet);
            }

            ToggleDisplaySet(false);
        }

        /// <summary>
        /// Tell the Lobby Manager That Your ReadyAnd Update sync var at SERVER!!!
        /// </summary>
        public void BtnPlayerReady()
        {
            if (isLocalPlayer == false)
            {
                Debug.LogError("CustomLobbyPlayer:A Noon Local player cannot have this button enabled!");
            }
            m_myPlayerScoreSet.m_strName = m_refNameInputField.text;
            m_GameDetails._LocalPlayer = m_myPlayerScoreSet;
            Debug.Log("CustomLobbyPlayer:I am ready" + name + ",Player Name" + m_myPlayerScoreSet.m_strName);
            CmdPlayerReady(m_refNameInputField.text);
            SendReadyToBeginMessage();
            //REMEBER AT CALLBACK THE TEXT IS SWITCHED OFFFF>>>
        }

        #region CALLS
        [Command]
        public void CmdPlayerReady(string a_strPlayerName)
        {

            m_strPlayerName = a_strPlayerName;
            //((int)GetComponent<NetworkIdentity>().netId.Value)
            //connectionToClient.connectionId
            EventPlayerReadyFromServer(((int)GetComponent<NetworkIdentity>().netId.Value).ToString(),m_strPlayerName,BogCommonHelper.IPManager.GetLocalIPAddress());///***************
            Debug.Log("CustomLobbyPlayer:Local PLayer Says ready from his orgin...." + a_strPlayerName + ",Info,ISLocalplayer:" + isLocalPlayer);
            //In case odf dedicated server...
            ToggleDisplaySet(isLocalPlayer);
        }


        [Command]
        public void CmdUpdateGameDetailsFromServer()
        {
            //Decide only on server...
            int temp_iSeed = 5252;
            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
          
            m_GameDetails._GameSeed = cur_time;
            RpcUpdateGameDetailsFromServer(cur_time);
            Debug.Log("CmdUpdateGameDetailsFromServer:Sending Game Seed to everybody"+ m_GameDetails._GameSeed);
         
        }
        [ClientRpc]
        public void RpcUpdateGameDetailsFromServer(int a_iGameSeed)
        {
            m_GameDetails._GameSeed = a_iGameSeed;
            UnityEngine.Random.InitState(a_iGameSeed);
            Debug.LogError("RpcUpdateGameDetailsFromServer:Rcvd Game Seed from server and updated!" + a_iGameSeed);
        }

        #endregion CALLS


    }
}