﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SIDZ.BogGame.Core
{
    public class BogPointer : MonoBehaviour, IStateObject
    {
        public enum ePointerState
        {
            None,
            Press,
            Hold,
            Released,
        }
        public enum ePointerLocation
        {
            InsideBoard,
            OutsideBoard
        }

        [Header("Game Details")]
        [SerializeField] private GameDetails m_refGameDetails;

        [Header("Settings:BogPointer")]
        [SerializeField] private BogCommonHelper.eActiveState m_eCurrentActiveState = BogCommonHelper.eActiveState.Enabled;
        [SerializeField] private ePointerState m_eCurrrentPointerState = ePointerState.None;
        [SerializeField] private ePointerLocation m_eCurrentPointerLocation = ePointerLocation.OutsideBoard;

        [Header("Settings:Selection")]
        [SerializeField] private List<BogDice> m_lstSelectedDice;
        [SerializeField] private List<string> m_lstSelectedDiceValue;
        [SerializeField] private Text m_DisplayText;
        private bool m_bBeginCheck = false;

        [Header("Settings:Word List Check")]
        [SerializeField] private BogResourceHolder m_refBogResourceHolder;

        [Header("Audio_LAzy to keep it seperate")]
        [SerializeField] private AudioSource m_refAudioSSource;
        [SerializeField] private AudioClip m_sfxCorrectWord;
        [SerializeField] private AudioClip m_sfxWrongWord;


        public bool IsActiveState()
        {
            return (m_eCurrentActiveState == BogCommonHelper.eActiveState.Enabled);

        }

        public BogCommonHelper.eActiveState GetCurrentState()
        {
            return m_eCurrentActiveState;
        }
        public void SetActiveState(BogCommonHelper.eActiveState a_eCurrentActiveState)
        {
            if (a_eCurrentActiveState == m_eCurrentActiveState)
            {
                Debug.Log("Same Active State:" + m_eCurrentActiveState);
                return;
            }
            m_eCurrentActiveState = a_eCurrentActiveState;
        }

    
        // Use this for initialization
        void Start()
        {
            //  Cursor.visible = false;
         


        }

        // Update is called once per frame
        void Update()
        {
           
            if (Input.GetMouseButtonDown(0))
            {
                UpdatePointerState(ePointerState.Press);
            }
            else if (Input.GetMouseButton(0))
            {
                UpdatePointerState(ePointerState.Hold);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                UpdatePointerState(ePointerState.Released);
            }
            else
            {
                UpdatePointerState(ePointerState.None);
            }

            NextStateCheck();

        }

        [ContextMenu("Check word with dictionary1")]
        public bool CheckValidWord(string a_strWord)
        {
            if (m_lstSelectedDiceValue == null || m_lstSelectedDiceValue.Count <= 0 || m_refBogResourceHolder == null)
            {
                Debug.LogError("Not Enough letters Selected.!!!");
                return false;
            }
            bool temp_bResult = true;
            string temp_strLetter = "";

            temp_strLetter = a_strWord.ToLower();
            temp_bResult = m_refBogResourceHolder.ValidWord(temp_strLetter);
            Debug.LogError("Word " + temp_strLetter + ",is " + temp_bResult);
            return
            temp_bResult;
        }

        private void CheckSelectedLetters()
        {
            string temp_strLetter = "";

            //Incase player had some selected when state change
            //Even a way to reject it before all this
            if (IsActiveState() == true)
            {
                foreach (string temp_diceValue in m_lstSelectedDiceValue)
                {
                    temp_strLetter += temp_diceValue;
                }
                if (CheckValidWord(temp_strLetter) == true)
                {
                    BogEventManager._Instance.Fire_evntOnUI_CorrectWord(temp_strLetter);

                }
                else if(temp_strLetter!="")//When atleast something is sslelected
                {
                    m_refAudioSSource.clip = m_sfxWrongWord;
                    m_refAudioSSource.Play();
                }
            }
            m_lstSelectedDiceValue.Clear();

            //Setting back the dice to its orginal state....
            foreach (BogDice temp_bogDice in m_lstSelectedDice)
            {
                //    temp_bogDice.transform.localScale *= (1.0f / 0.85f);
                temp_bogDice.ChangeDiceState(BogDice.eDiceState.eUnSelected);
            }
            m_lstSelectedDice.Clear();
            DisplayUIText();
        }
        private void NextStateCheck()
        {
            if (m_eCurrrentPointerState == ePointerState.Press)// && m_eCurrentPointerLocation == ePointerLocation.InsideBoard)
            {
                m_bBeginCheck = true;

            }
            if (m_eCurrrentPointerState == ePointerState.Released)
            {
                CheckSelectedLetters();


            }
            if (m_eCurrrentPointerState == ePointerState.Released || m_eCurrrentPointerState == ePointerState.None)
            {
                m_bBeginCheck = false;

            }
        }

        private void UpdatePointerState(ePointerState a_ePointerState)
        {
            if (a_ePointerState == m_eCurrrentPointerState)
            {
                return;
            }
            m_eCurrrentPointerState = a_ePointerState;
            // Debug.Log("UpdatePointerState:" + m_eCurrrentPointerState);

        }
        private void UpdatePointerLocState(ePointerLocation a_ePointerLocation)
        {
            if (a_ePointerLocation == m_eCurrentPointerLocation)
            {
                return;
            }
            m_eCurrentPointerLocation = a_ePointerLocation;
            //   Debug.Log("UpdatePointerLocState:" + m_eCurrentPointerLocation);
        }

        BogDice m_refBogDice = null;
        private void DisplayUIText()
        {
            string temp_strDisplay = "";
            foreach (string temp_diceValue in m_lstSelectedDiceValue)
            {
                temp_strDisplay += temp_diceValue;
            }
            m_DisplayText.text = temp_strDisplay;
        }
        private void LogicOnDiceSelectEnd(Collider other)
        {

            if (IsActiveState() == false)
            {
                return;
            }

            if (m_bBeginCheck == false)
            {
                //    Debug.Log("Not Check logic");
                return;
            }
            m_refBogDice = other.GetComponent<BogDice>();
            if (m_refBogDice == null)
            {
                return;
            }
            if (m_lstSelectedDice.Contains(m_refBogDice) == false)
            {
                // m_refBogDice.transform.localScale *= 0.85f;
                m_refBogDice.ChangeDiceState(BogDice.eDiceState.eSelected);
                m_lstSelectedDice.Add(m_refBogDice);
                m_lstSelectedDiceValue.Add(m_refBogDice.DiceValue);
            }
            else
            {
                int iIndexOf = m_lstSelectedDice.IndexOf(m_refBogDice);
                if(iIndexOf+1 < m_lstSelectedDice.Count)
                {
                    m_lstSelectedDice[iIndexOf + 1].ChangeDiceState(BogDice.eDiceState.eUnSelected);
                    m_lstSelectedDice.RemoveAt(iIndexOf + 1);
                    m_lstSelectedDiceValue.RemoveAt(iIndexOf + 1);
                }
            }
            DisplayUIText();


        }




        void OnTriggerEnter(Collider other)
        {
            if (other.name == "BogBoard")
            {
                UpdatePointerLocState(ePointerLocation.InsideBoard);
            }
            LogicOnDiceSelectEnd(other);

        }
        void OnTriggerExit(Collider other)
        {
            if (other.name == "BogBoard")
            {
                UpdatePointerLocState(ePointerLocation.OutsideBoard);
            }
        }


    }
}