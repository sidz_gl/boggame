﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SIDZ.BogGame.Core
{
    public class BogMuteButton : MonoBehaviour
    {
        [Header("Read Only")]
       [SerializeField] private bool m_bCurrentState = true;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ToggleState()
        {
            m_bCurrentState = !m_bCurrentState;
            if(BogAudioManager._Instance)
            {
                BogAudioManager._Instance.SetMuteAudioListener(m_bCurrentState);
            }
        }
    }
}